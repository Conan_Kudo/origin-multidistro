#debuginfo not supported with Go
%global debug_package %{nil}
# modifying the Go binaries breaks the DWARF debugging
%global __os_install_post %{_rpmconfigdir}/brp-compress

%global gopath      %{_datadir}/gocode
%global import_path github.com/openshift/origin
%global registry_import_path github.com/openshift/image-registry
%global webconsole_import_path github.com/openshift/origin-web-console-server
# The following should only be used for cleanup of sdn-ovs upgrades
%global kube_plugin_path %{_libexecdir}/kubernetes/kubelet-plugins/net/exec/redhat~openshift-ovs-subnet

# docker_version is the version of docker requires by packages
%global docker_version 1.12
# openvswitch_version is the version of openvswitch requires by packages
%global openvswitch_version 2.6.1
# this is the version we obsolete up to. The packaging changed for Origin
# 1.0.6 and OSE 3.1 such that 'openshift' package names were no longer used.
%global package_refector_version 3.0.2.900
%global golang_version 1.6.2
# %%commit and %%os_git_vars are intended to be set by tito custom builders provided
# in the .tito/lib directory. The values in this spec file will not be kept up to date.
%{!?commit:
%global commit 191fece9305a76f262baacc9de72c2c8cb4d5601
}
%global kube_commit cbc5b493627e993e4e4f02301702c16ae28ea88f
%global etcd_commit 121edf0467052d55876a817b89875fb39a99bf78
%global registry_commit fef8b8b5ff6c348ff3efdd518398314234587d8e
%global webconsole_commit b600d46af2b38b09380e70fd696e2c921404263d

%global shortcommit %(c=%{commit}; echo ${c:0:7})
%global kube_shortcommit %(c=%{kube_commit}; echo ${c:0:7})
%global etcd_shortcommit %(c=%{etcd_commit}; echo ${c:0:7})
%global registry_shortcommit %(c=%{registry_commit}; echo ${c:0:7})
%global webconsole_shortcommit %(c=%{webconsole_commit}; echo ${c:0:7})

# os_git_vars needed to run hack scripts during rpm builds
# place to look for the kube, catalog and etcd commit hashes are the lock files in the origin tree, seems that origin build scripts are ignorant about what origin is bundling...
%{!?os_git_vars:
%global os_git_vars OS_GIT_VERSION=v3.9.0 OS_GIT_MINOR=9+ OS_GIT_COMMIT=%{shortcommit} OS_GIT_MAJOR=3 OS_GIT_TREE_STATE=clean KUBE_GIT_VERSION=v1.9.1+%{kube_shortcommit} KUBE_GIT_COMMIT=%{kube_shortcommit} ETCD_GIT_COMMIT=%{etcd_shortcommit} ETCD_GIT_VERSION=v3.2.16 OS_GIT_CATALOG_VERSION=v0.1.9
}

%if 0%{?fedora} || 0%{?epel} || 0%{?mageia} || 0%{?suse_version}
%global need_redistributable_set 0
%else
# Due to library availability, redistributable builds only work on x86_64
%ifarch x86_64
%global need_redistributable_set 1
%else
%global need_redistributable_set 0
%endif
%endif
%{!?make_redistributable: %global make_redistributable %{need_redistributable_set}}

# by default build the test binaries for Origin
%{!?build_tests: %global build_tests 1 }

%if "%{dist}" == ".el7aos"
%global package_name atomic-openshift
%global product_name Atomic OpenShift
%else
%global package_name origin
%global product_name Origin
%endif

Name:           %{package_name}
# Version is not kept up to date and is intended to be set by tito custom
# builders provided in the .tito/lib directory of this project
Version:        3.9.0
Release:        0.0.1%{?dist}
Summary:        OpenShift Open Source Container Management by Red Hat
License:        ASL 2.0
URL:            https://%{import_path}

%if 0%{?suse_version}
%{?go_exclusivearch:%go_exclusivearch}%{!?go_exclusivearch:ExclusiveArch:  x86_64 aarch64 ppc64le s390x}
%else
# RH/Fedora ExclusiveArch
# If go_arches not defined fall through to implicit golang archs
%if 0%{?go_arches:1}
ExclusiveArch:  %{go_arches}
%else
ExclusiveArch:  x86_64 aarch64 ppc64le s390x
%endif
%endif

# Current bug in golang which causes runtime error on armv7hl for the gendoc
# util which causes build failure because docs can not be generated.
#
#   https://bugzilla.redhat.com/show_bug.cgi?id=1445946
#
# Also exclude ppc64 since docker doesn't exist for it.
#ExcludeArch: %{arm} ppc64

# Let's try armv7hl again for rawhide and origin 3.6.0
ExcludeArch: ppc64

Source0:        https://%{import_path}/archive/%{commit}/%{name}-%{version}.tar.gz
# Docker registry has been moved to separate repository at https://github.com/openshift/image-registry
Source1:        https://%{registry_import_path}/archive/%{registry_commit}/v%{version}/%{name}-image-registry-%{version}.tar.gz
Source2:        https://%{webconsole_import_path}/archive/%{webconsole_commit}/%{name}-webconsole-v%{version}.tar.gz

# Patch to enable armv7hl and i386
#
# armv7hl parts submitted upstream:
#   https://github.com/openshift/origin/pull/15686
#
# Upstream had this explcitly disabled for i386 but had client builds enabled.
# Will follow up with upstream to find out if they want to leave this as is.
Patch0:         origin-3.6.0-build.patch
Patch1:         const-overflow-fix.patch

%if 0%{?suse_version}
# SUSE splits up systemd macros into separate package
BuildRequires:  systemd-rpm-macros
%else
# RH/Fedora and Mageia keep systemd.pc with systemd macros
BuildRequires:  pkgconfig(systemd)
%endif
BuildRequires:  bsdtar
%if 0%{?suse_version}
BuildRequires:  go >= %{golang_version}
BuildRequires:  golang-packaging
%else
BuildRequires:  golang >= %{golang_version}
%endif
BuildRequires:  krb5-devel
BuildRequires:  rsync
Requires:       %{name}-clients = %{version}-%{release}
Requires:       iptables
Obsoletes:      openshift < %{package_refector_version}

#
# The following Bundled Provides entries are populated automatically by the
# OpenShift Origin tito custom builder found here:
#   https://github.com/openshift/origin/blob/master/.tito/lib/origin/builder/
#
# Can also be generated with the following:
#   $ python -c 'import json; print "\n".join(["Provides: bundled(golang({})) = {}".format(dep[u"ImportPath"], dep[u"Rev"]) for dep in json.load(open("Godeps/Godeps.json", "r"))[u"Deps"]])'
#
# These are defined as per:
# https://fedoraproject.org/wiki/Packaging:Guidelines#Bundling_and_Duplication_of_system_libraries
Provides: bundled(golang(bitbucket.org/bertimus9/systemstat)) = 1468fd0db20598383c9393cccaa547de6ad99e5e
Provides: bundled(golang(bitbucket.org/ww/goautoneg)) = 75cd24fc2f2c2a2088577d12123ddee5f54e0675
Provides: bundled(golang(cloud.google.com/go/compute/metadata)) = 3b1ae45394a234c385be014e9a488f2bb6eef821
Provides: bundled(golang(cloud.google.com/go/internal)) = 3b1ae45394a234c385be014e9a488f2bb6eef821
Provides: bundled(golang(github.com/AaronO/go-git-http)) = 34209cf6cd947cfa52063bcb0f6d43cfa50c5566
Provides: bundled(golang(github.com/AaronO/go-git-http/auth)) = 34209cf6cd947cfa52063bcb0f6d43cfa50c5566
Provides: bundled(golang(github.com/Azure/azure-sdk-for-go/arm/compute)) = 0984e0641ae43b89283223034574d6465be93bf4
Provides: bundled(golang(github.com/Azure/azure-sdk-for-go/arm/containerregistry)) = 0984e0641ae43b89283223034574d6465be93bf4
Provides: bundled(golang(github.com/Azure/azure-sdk-for-go/arm/network)) = 0984e0641ae43b89283223034574d6465be93bf4
Provides: bundled(golang(github.com/Azure/azure-sdk-for-go/arm/storage)) = 0984e0641ae43b89283223034574d6465be93bf4
Provides: bundled(golang(github.com/Azure/azure-sdk-for-go/storage)) = 0984e0641ae43b89283223034574d6465be93bf4
Provides: bundled(golang(github.com/Azure/go-ansiterm)) = 7e0a0b69f76673d5d2f451ee59d9d02cfa006527
Provides: bundled(golang(github.com/Azure/go-ansiterm/winterm)) = 7e0a0b69f76673d5d2f451ee59d9d02cfa006527
Provides: bundled(golang(github.com/Azure/go-autorest/autorest)) = d7c034a8af24eda120dd6460bfcd6d9ed14e43ca
Provides: bundled(golang(github.com/Azure/go-autorest/autorest/azure)) = d7c034a8af24eda120dd6460bfcd6d9ed14e43ca
Provides: bundled(golang(github.com/Azure/go-autorest/autorest/date)) = d7c034a8af24eda120dd6460bfcd6d9ed14e43ca
Provides: bundled(golang(github.com/Azure/go-autorest/autorest/to)) = d7c034a8af24eda120dd6460bfcd6d9ed14e43ca
Provides: bundled(golang(github.com/Azure/go-autorest/autorest/validation)) = d7c034a8af24eda120dd6460bfcd6d9ed14e43ca
Provides: bundled(golang(github.com/MakeNowJust/heredoc)) = 1d91351acdc1cb2f2c995864674b754134b86ca7
Provides: bundled(golang(github.com/Microsoft/go-winio)) = 24a3e3d3fc7451805e09d11e11e95d9a0a4f205e
Provides: bundled(golang(github.com/Nvveen/Gotty)) = cd527374f1e5bff4938207604a14f2e38a9cf512
Provides: bundled(golang(github.com/PuerkitoBio/purell)) = 8a290539e2e8629dbc4e6bad948158f790ec31f4
Provides: bundled(golang(github.com/PuerkitoBio/urlesc)) = 5bd2802263f21d8788851d5305584c82a5c75d7e
Provides: bundled(golang(github.com/RangelReale/osin)) = 1c1a533224dd9c631fdd8df8851b167d24cabe96
Provides: bundled(golang(github.com/RangelReale/osincli)) = fababb0555f21315d1a34af6615a16eaab44396b
Provides: bundled(golang(github.com/Sirupsen/logrus)) = aaf92c95712104318fc35409745f1533aa5ff327
Provides: bundled(golang(github.com/Sirupsen/logrus/formatters/logstash)) = aaf92c95712104318fc35409745f1533aa5ff327
Provides: bundled(golang(github.com/abbot/go-http-auth)) = c0ef4539dfab4d21c8ef20ba2924f9fc6f186d35
Provides: bundled(golang(github.com/apcera/gssapi)) = b28cfdd5220f7ebe15d8372ac81a7f41cc35ab32
Provides: bundled(golang(github.com/appc/spec/schema)) = fc380db5fc13c6dd71a5b0bf2af0d182865d1b1d
Provides: bundled(golang(github.com/appc/spec/schema/common)) = fc380db5fc13c6dd71a5b0bf2af0d182865d1b1d
Provides: bundled(golang(github.com/appc/spec/schema/types)) = fc380db5fc13c6dd71a5b0bf2af0d182865d1b1d
Provides: bundled(golang(github.com/appc/spec/schema/types/resource)) = fc380db5fc13c6dd71a5b0bf2af0d182865d1b1d
Provides: bundled(golang(github.com/armon/circbuf)) = bbbad097214e2918d8543d5201d12bfd7bca254d
Provides: bundled(golang(github.com/asaskevich/govalidator)) = 593d64559f7600f29581a3ee42177f5dbded27a9
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/awserr)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/awsutil)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/client)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/client/metadata)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/corehandlers)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/credentials)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/credentials/ec2rolecreds)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/credentials/endpointcreds)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/credentials/stscreds)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/defaults)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/ec2metadata)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/endpoints)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/request)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/session)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/aws/signer/v4)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/awstesting)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/awstesting/unit)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol/ec2query)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol/json/jsonutil)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol/jsonrpc)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol/query)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol/query/queryutil)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol/rest)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol/restxml)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/protocol/xml/xmlutil)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/util)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/private/waiter)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/service/autoscaling)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/service/cloudfront/sign)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/service/ec2)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/service/ecr)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/service/elb)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/service/route53)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/service/s3)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/aws/aws-sdk-go/service/sts)) = 63ce630574a5ec05ecd8e8de5cea16332a5a684d
Provides: bundled(golang(github.com/beorn7/perks/quantile)) = 3ac7bf7a47d159a033b107610db8a1b6575507a4
Provides: bundled(golang(github.com/blang/semver)) = 31b736133b98f26d5e078ec9eb591666edfd091f
Provides: bundled(golang(github.com/boltdb/bolt)) = 583e8937c61f1af6513608ccc75c97b6abdf4ff9
Provides: bundled(golang(github.com/chai2010/gettext-go/gettext)) = c6fed771bfd517099caf0f7a961671fa8ed08723
Provides: bundled(golang(github.com/chai2010/gettext-go/gettext/mo)) = c6fed771bfd517099caf0f7a961671fa8ed08723
Provides: bundled(golang(github.com/chai2010/gettext-go/gettext/plural)) = c6fed771bfd517099caf0f7a961671fa8ed08723
Provides: bundled(golang(github.com/chai2010/gettext-go/gettext/po)) = c6fed771bfd517099caf0f7a961671fa8ed08723
Provides: bundled(golang(github.com/cloudflare/cfssl/auth)) = fca70798646c8689aeae5928d4ad1278ff8a3c17
Provides: bundled(golang(github.com/cloudflare/cfssl/certdb)) = fca70798646c8689aeae5928d4ad1278ff8a3c17
Provides: bundled(golang(github.com/cloudflare/cfssl/config)) = fca70798646c8689aeae5928d4ad1278ff8a3c17
Provides: bundled(golang(github.com/cloudflare/cfssl/crypto/pkcs7)) = fca70798646c8689aeae5928d4ad1278ff8a3c17
Provides: bundled(golang(github.com/cloudflare/cfssl/csr)) = fca70798646c8689aeae5928d4ad1278ff8a3c17
Provides: bundled(golang(github.com/cloudflare/cfssl/errors)) = fca70798646c8689aeae5928d4ad1278ff8a3c17
Provides: bundled(golang(github.com/cloudflare/cfssl/helpers)) = fca70798646c8689aeae5928d4ad1278ff8a3c17
Provides: bundled(golang(github.com/cloudflare/cfssl/helpers/derhelpers)) = fca70798646c8689aeae5928d4ad1278ff8a3c17
Provides: bundled(golang(github.com/cloudflare/cfssl/info)) = fca70798646c8689aeae5928d4ad1278ff8a3c17
Provides: bundled(golang(github.com/cloudflare/cfssl/log)) = fca70798646c8689aeae5928d4ad1278ff8a3c17
Provides: bundled(golang(github.com/cloudflare/cfssl/ocsp/config)) = fca70798646c8689aeae5928d4ad1278ff8a3c17
Provides: bundled(golang(github.com/cloudflare/cfssl/signer)) = fca70798646c8689aeae5928d4ad1278ff8a3c17
Provides: bundled(golang(github.com/cloudflare/cfssl/signer/local)) = fca70798646c8689aeae5928d4ad1278ff8a3c17
Provides: bundled(golang(github.com/clusterhq/flocker-go)) = 2b8b7259d3139c96c4a6871031355808ab3fd3b3
Provides: bundled(golang(github.com/cockroachdb/cmux)) = 3bbbe9847675a1300cce193d9efe458b9f0bdd23
Provides: bundled(golang(github.com/codedellemc/goscaleio)) = 8ed64a07d23f79bab973f1630651841ccc656887
Provides: bundled(golang(github.com/codedellemc/goscaleio/types/v1)) = 8ed64a07d23f79bab973f1630651841ccc656887
Provides: bundled(golang(github.com/codegangsta/negroni)) = 8d75e11374a1928608c906fe745b538483e7aeb2
Provides: bundled(golang(github.com/containernetworking/cni/libcni)) = 52e4358cbd540cc31f72ea5e0bd4762c98011b84
Provides: bundled(golang(github.com/containernetworking/cni/pkg/invoke)) = 52e4358cbd540cc31f72ea5e0bd4762c98011b84
Provides: bundled(golang(github.com/containernetworking/cni/pkg/ip)) = 52e4358cbd540cc31f72ea5e0bd4762c98011b84
Provides: bundled(golang(github.com/containernetworking/cni/pkg/ipam)) = 52e4358cbd540cc31f72ea5e0bd4762c98011b84
Provides: bundled(golang(github.com/containernetworking/cni/pkg/ns)) = 52e4358cbd540cc31f72ea5e0bd4762c98011b84
Provides: bundled(golang(github.com/containernetworking/cni/pkg/skel)) = 52e4358cbd540cc31f72ea5e0bd4762c98011b84
Provides: bundled(golang(github.com/containernetworking/cni/pkg/types)) = 52e4358cbd540cc31f72ea5e0bd4762c98011b84
Provides: bundled(golang(github.com/containernetworking/cni/pkg/utils/hwaddr)) = 52e4358cbd540cc31f72ea5e0bd4762c98011b84
Provides: bundled(golang(github.com/containernetworking/cni/pkg/version)) = 52e4358cbd540cc31f72ea5e0bd4762c98011b84
Provides: bundled(golang(github.com/containernetworking/cni/plugins/ipam/host-local)) = 52e4358cbd540cc31f72ea5e0bd4762c98011b84
Provides: bundled(golang(github.com/containernetworking/cni/plugins/ipam/host-local/backend)) = 52e4358cbd540cc31f72ea5e0bd4762c98011b84
Provides: bundled(golang(github.com/containernetworking/cni/plugins/ipam/host-local/backend/disk)) = 52e4358cbd540cc31f72ea5e0bd4762c98011b84
Provides: bundled(golang(github.com/containernetworking/cni/plugins/ipam/host-local/backend/testing)) = 52e4358cbd540cc31f72ea5e0bd4762c98011b84
Provides: bundled(golang(github.com/containernetworking/cni/plugins/main/loopback)) = 52e4358cbd540cc31f72ea5e0bd4762c98011b84
Provides: bundled(golang(github.com/containers/image/docker/policyconfiguration)) = f5768e7cbd2d715ea3f0153cd857699157d1f33a
Provides: bundled(golang(github.com/containers/image/docker/reference)) = f5768e7cbd2d715ea3f0153cd857699157d1f33a
Provides: bundled(golang(github.com/containers/image/manifest)) = f5768e7cbd2d715ea3f0153cd857699157d1f33a
Provides: bundled(golang(github.com/containers/image/signature)) = f5768e7cbd2d715ea3f0153cd857699157d1f33a
Provides: bundled(golang(github.com/containers/image/transports)) = f5768e7cbd2d715ea3f0153cd857699157d1f33a
Provides: bundled(golang(github.com/containers/image/types)) = f5768e7cbd2d715ea3f0153cd857699157d1f33a
Provides: bundled(golang(github.com/containers/image/version)) = f5768e7cbd2d715ea3f0153cd857699157d1f33a
Provides: bundled(golang(github.com/containers/storage/pkg/homedir)) = 5cbbc6bafb45bd7ef10486b673deb3b81bb3b787
Provides: bundled(golang(github.com/coreos/etcd/alarm)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/auth)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/auth/authpb)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/client)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/clientv3)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/clientv3/concurrency)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/clientv3/namespace)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/clientv3/naming)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/compactor)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/discovery)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/embed)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/error)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/etcdserver)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/etcdserver/api)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/etcdserver/api/v2http)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/etcdserver/api/v2http/httptypes)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/etcdserver/api/v3client)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/etcdserver/api/v3election)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/etcdserver/api/v3election/v3electionpb)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/etcdserver/api/v3election/v3electionpb/gw)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/etcdserver/api/v3lock)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/etcdserver/api/v3lock/v3lockpb)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/etcdserver/api/v3lock/v3lockpb/gw)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/etcdserver/api/v3rpc)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/etcdserver/api/v3rpc/rpctypes)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/etcdserver/auth)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/etcdserver/etcdserverpb)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/etcdserver/etcdserverpb/gw)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/etcdserver/membership)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/etcdserver/stats)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/integration)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/lease)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/lease/leasehttp)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/lease/leasepb)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/mvcc)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/mvcc/backend)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/mvcc/mvccpb)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/adt)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/contention)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/cors)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/cpuutil)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/crc)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/debugutil)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/fileutil)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/httputil)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/idutil)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/ioutil)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/logutil)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/monotime)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/netutil)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/osutil)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/pathutil)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/pbutil)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/runtime)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/schedule)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/srv)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/testutil)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/tlsutil)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/transport)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/types)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/pkg/wait)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/proxy/grpcproxy)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/proxy/grpcproxy/adapter)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/proxy/grpcproxy/cache)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/raft)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/raft/raftpb)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/rafthttp)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/snap)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/snap/snappb)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/store)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/version)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/wal)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/etcd/wal/walpb)) = 61fc123e7a8b14a0a258aa3f5c4159861b1ec2e7
Provides: bundled(golang(github.com/coreos/go-iptables/iptables)) = fbb73372b87f6e89951c2b6b31470c2c9d5cfae3
Provides: bundled(golang(github.com/coreos/go-oidc/http)) = be73733bb8cc830d0205609b95d125215f8e9c70
Provides: bundled(golang(github.com/coreos/go-oidc/jose)) = be73733bb8cc830d0205609b95d125215f8e9c70
Provides: bundled(golang(github.com/coreos/go-oidc/key)) = be73733bb8cc830d0205609b95d125215f8e9c70
Provides: bundled(golang(github.com/coreos/go-oidc/oauth2)) = be73733bb8cc830d0205609b95d125215f8e9c70
Provides: bundled(golang(github.com/coreos/go-oidc/oidc)) = be73733bb8cc830d0205609b95d125215f8e9c70
Provides: bundled(golang(github.com/coreos/go-semver/semver)) = d043ae190b3202550d026daf009359bb5d761672
Provides: bundled(golang(github.com/coreos/go-systemd/daemon)) = 4484981625c1a6a2ecb40a390fcb6a9bcfee76e3
Provides: bundled(golang(github.com/coreos/go-systemd/dbus)) = 4484981625c1a6a2ecb40a390fcb6a9bcfee76e3
Provides: bundled(golang(github.com/coreos/go-systemd/journal)) = 4484981625c1a6a2ecb40a390fcb6a9bcfee76e3
Provides: bundled(golang(github.com/coreos/go-systemd/unit)) = 4484981625c1a6a2ecb40a390fcb6a9bcfee76e3
Provides: bundled(golang(github.com/coreos/go-systemd/util)) = 4484981625c1a6a2ecb40a390fcb6a9bcfee76e3
Provides: bundled(golang(github.com/coreos/pkg/capnslog)) = fa29b1d70f0beaddd4c7021607cc3c3be8ce94b8
Provides: bundled(golang(github.com/coreos/pkg/dlopen)) = fa29b1d70f0beaddd4c7021607cc3c3be8ce94b8
Provides: bundled(golang(github.com/coreos/pkg/health)) = fa29b1d70f0beaddd4c7021607cc3c3be8ce94b8
Provides: bundled(golang(github.com/coreos/pkg/httputil)) = fa29b1d70f0beaddd4c7021607cc3c3be8ce94b8
Provides: bundled(golang(github.com/coreos/pkg/timeutil)) = fa29b1d70f0beaddd4c7021607cc3c3be8ce94b8
Provides: bundled(golang(github.com/coreos/rkt/api/v1alpha)) = a83419be28ac626876f94a28b4df2dbc9eac7448
Provides: bundled(golang(github.com/cpuguy83/go-md2man/md2man)) = 71acacd42f85e5e82f70a55327789582a5200a90
Provides: bundled(golang(github.com/davecgh/go-spew/spew)) = 5215b55f46b2b919f50a1df0eaa5886afe4e3b3d
Provides: bundled(golang(github.com/daviddengcn/go-colortext)) = 511bcaf42ccd42c38aba7427b6673277bf19e2a1
Provides: bundled(golang(github.com/denverdino/aliyungo/common)) = 6670d44234961c0ca4c377c6b6270c3594ceb386
Provides: bundled(golang(github.com/denverdino/aliyungo/oss)) = 6670d44234961c0ca4c377c6b6270c3594ceb386
Provides: bundled(golang(github.com/denverdino/aliyungo/util)) = 6670d44234961c0ca4c377c6b6270c3594ceb386
Provides: bundled(golang(github.com/dgrijalva/jwt-go)) = 01aeca54ebda6e0fbfafd0a524d234159c05ec20
Provides: bundled(golang(github.com/docker/distribution)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/configuration)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/context)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/digest)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/health)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/health/checks)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/manifest)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/manifest/manifestlist)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/manifest/schema1)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/manifest/schema2)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/notifications)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/reference)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/api/errcode)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/api/v2)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/auth)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/auth/htpasswd)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/auth/token)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/client)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/client/auth)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/client/transport)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/handlers)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/middleware/registry)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/middleware/repository)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/proxy)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/proxy/scheduler)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/storage)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/storage/cache)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/storage/cache/memory)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/storage/cache/redis)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/storage/driver)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/storage/driver/azure)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/storage/driver/base)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/storage/driver/factory)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/storage/driver/filesystem)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/storage/driver/gcs)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/storage/driver/inmemory)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/storage/driver/middleware)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/storage/driver/middleware/cloudfront)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/storage/driver/oss)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/storage/driver/s3-aws)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/registry/storage/driver/swift)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/uuid)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/distribution/version)) = 12acdf0a6c1e56d965ac6eb395d2bce687bf22fc
Provides: bundled(golang(github.com/docker/docker/builder/dockerfile/command)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/docker/builder/dockerfile/parser)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/docker/cliconfig)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/docker/opts)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/docker/pkg/archive)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/docker/pkg/fileutils)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/docker/pkg/homedir)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/docker/pkg/idtools)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/docker/pkg/ioutils)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/docker/pkg/jsonlog)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/docker/pkg/jsonmessage)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/docker/pkg/longpath)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/docker/pkg/mount)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/docker/pkg/pools)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/docker/pkg/promise)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/docker/pkg/stdcopy)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/docker/pkg/symlink)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/docker/pkg/system)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/docker/pkg/term)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/docker/pkg/term/windows)) = b9f10c951893f9a00865890a5232e85d770c1087
Provides: bundled(golang(github.com/docker/engine-api/client)) = dea108d3aa0c67d7162a3fd8aa65f38a430019fd
Provides: bundled(golang(github.com/docker/engine-api/client/transport)) = dea108d3aa0c67d7162a3fd8aa65f38a430019fd
Provides: bundled(golang(github.com/docker/engine-api/client/transport/cancellable)) = dea108d3aa0c67d7162a3fd8aa65f38a430019fd
Provides: bundled(golang(github.com/docker/engine-api/types)) = dea108d3aa0c67d7162a3fd8aa65f38a430019fd
Provides: bundled(golang(github.com/docker/engine-api/types/blkiodev)) = dea108d3aa0c67d7162a3fd8aa65f38a430019fd
Provides: bundled(golang(github.com/docker/engine-api/types/container)) = dea108d3aa0c67d7162a3fd8aa65f38a430019fd
Provides: bundled(golang(github.com/docker/engine-api/types/filters)) = dea108d3aa0c67d7162a3fd8aa65f38a430019fd
Provides: bundled(golang(github.com/docker/engine-api/types/network)) = dea108d3aa0c67d7162a3fd8aa65f38a430019fd
Provides: bundled(golang(github.com/docker/engine-api/types/reference)) = dea108d3aa0c67d7162a3fd8aa65f38a430019fd
Provides: bundled(golang(github.com/docker/engine-api/types/registry)) = dea108d3aa0c67d7162a3fd8aa65f38a430019fd
Provides: bundled(golang(github.com/docker/engine-api/types/strslice)) = dea108d3aa0c67d7162a3fd8aa65f38a430019fd
Provides: bundled(golang(github.com/docker/engine-api/types/time)) = dea108d3aa0c67d7162a3fd8aa65f38a430019fd
Provides: bundled(golang(github.com/docker/engine-api/types/versions)) = dea108d3aa0c67d7162a3fd8aa65f38a430019fd
Provides: bundled(golang(github.com/docker/go-connections/nat)) = f549a9393d05688dff0992ef3efd8bbe6c628aeb
Provides: bundled(golang(github.com/docker/go-connections/sockets)) = f549a9393d05688dff0992ef3efd8bbe6c628aeb
Provides: bundled(golang(github.com/docker/go-connections/tlsconfig)) = f549a9393d05688dff0992ef3efd8bbe6c628aeb
Provides: bundled(golang(github.com/docker/go-units)) = e30f1e79f3cd72542f2026ceec18d3bd67ab859c
Provides: bundled(golang(github.com/docker/libtrust)) = c54fbb67c1f1e68d7d6f8d2ad7c9360404616a41
Provides: bundled(golang(github.com/docker/spdystream)) = 449fdfce4d962303d702fec724ef0ad181c92528
Provides: bundled(golang(github.com/docker/spdystream/spdy)) = 449fdfce4d962303d702fec724ef0ad181c92528
Provides: bundled(golang(github.com/elazarl/go-bindata-assetfs)) = 3dcc96556217539f50599357fb481ac0dc7439b9
Provides: bundled(golang(github.com/elazarl/goproxy)) = c4fc26588b6ef8af07a191fcb6476387bdd46711
Provides: bundled(golang(github.com/elazarl/goproxy/ext/image)) = 07b16b6e30fcac0ad8c0435548e743bcf2ca7e92
Provides: bundled(golang(github.com/elazarl/goproxy/regretable)) = 07b16b6e30fcac0ad8c0435548e743bcf2ca7e92
Provides: bundled(golang(github.com/emicklei/go-restful)) = 09691a3b6378b740595c1002f40c34dd5f218a22
Provides: bundled(golang(github.com/emicklei/go-restful/log)) = 09691a3b6378b740595c1002f40c34dd5f218a22
Provides: bundled(golang(github.com/emicklei/go-restful/swagger)) = 09691a3b6378b740595c1002f40c34dd5f218a22
Provides: bundled(golang(github.com/evanphx/json-patch)) = ba18e35c5c1b36ef6334cad706eb681153d2d379
Provides: bundled(golang(github.com/exponent-io/jsonpath)) = d6023ce2651d8eafb5c75bb0c7167536102ec9f5
Provides: bundled(golang(github.com/fatih/structs)) = a720dfa8df582c51dee1b36feabb906bde1588bd
Provides: bundled(golang(github.com/fsnotify/fsnotify)) = f12c6236fe7b5cf6bcf30e5935d08cb079d78334
Provides: bundled(golang(github.com/fsouza/go-dockerclient)) = 3f9370a4738ba8d0ed6eea63582ca6db5eb48032
Provides: bundled(golang(github.com/fsouza/go-dockerclient/external/github.com/Sirupsen/logrus)) = 3f9370a4738ba8d0ed6eea63582ca6db5eb48032
Provides: bundled(golang(github.com/fsouza/go-dockerclient/external/github.com/docker/docker/opts)) = 3f9370a4738ba8d0ed6eea63582ca6db5eb48032
Provides: bundled(golang(github.com/fsouza/go-dockerclient/external/github.com/docker/docker/pkg/archive)) = 3f9370a4738ba8d0ed6eea63582ca6db5eb48032
Provides: bundled(golang(github.com/fsouza/go-dockerclient/external/github.com/docker/docker/pkg/fileutils)) = 3f9370a4738ba8d0ed6eea63582ca6db5eb48032
Provides: bundled(golang(github.com/fsouza/go-dockerclient/external/github.com/docker/docker/pkg/homedir)) = 3f9370a4738ba8d0ed6eea63582ca6db5eb48032
Provides: bundled(golang(github.com/fsouza/go-dockerclient/external/github.com/docker/docker/pkg/idtools)) = 3f9370a4738ba8d0ed6eea63582ca6db5eb48032
Provides: bundled(golang(github.com/fsouza/go-dockerclient/external/github.com/docker/docker/pkg/ioutils)) = 3f9370a4738ba8d0ed6eea63582ca6db5eb48032
Provides: bundled(golang(github.com/fsouza/go-dockerclient/external/github.com/docker/docker/pkg/longpath)) = 3f9370a4738ba8d0ed6eea63582ca6db5eb48032
Provides: bundled(golang(github.com/fsouza/go-dockerclient/external/github.com/docker/docker/pkg/pools)) = 3f9370a4738ba8d0ed6eea63582ca6db5eb48032
Provides: bundled(golang(github.com/fsouza/go-dockerclient/external/github.com/docker/docker/pkg/promise)) = 3f9370a4738ba8d0ed6eea63582ca6db5eb48032
Provides: bundled(golang(github.com/fsouza/go-dockerclient/external/github.com/docker/docker/pkg/stdcopy)) = 3f9370a4738ba8d0ed6eea63582ca6db5eb48032
Provides: bundled(golang(github.com/fsouza/go-dockerclient/external/github.com/docker/docker/pkg/system)) = 3f9370a4738ba8d0ed6eea63582ca6db5eb48032
Provides: bundled(golang(github.com/fsouza/go-dockerclient/external/github.com/docker/go-units)) = 3f9370a4738ba8d0ed6eea63582ca6db5eb48032
Provides: bundled(golang(github.com/fsouza/go-dockerclient/external/github.com/hashicorp/go-cleanhttp)) = 3f9370a4738ba8d0ed6eea63582ca6db5eb48032
Provides: bundled(golang(github.com/fsouza/go-dockerclient/external/github.com/opencontainers/runc/libcontainer/user)) = 3f9370a4738ba8d0ed6eea63582ca6db5eb48032
Provides: bundled(golang(github.com/fsouza/go-dockerclient/external/golang.org/x/net/context)) = 3f9370a4738ba8d0ed6eea63582ca6db5eb48032
Provides: bundled(golang(github.com/fsouza/go-dockerclient/external/golang.org/x/sys/unix)) = 3f9370a4738ba8d0ed6eea63582ca6db5eb48032
Provides: bundled(golang(github.com/garyburd/redigo/internal)) = b8dc90050f24c1a73a52f107f3f575be67b21b7c
Provides: bundled(golang(github.com/garyburd/redigo/redis)) = b8dc90050f24c1a73a52f107f3f575be67b21b7c
Provides: bundled(golang(github.com/getsentry/raven-go)) = 86cd4063c535cbbcbf43d84424dbd5911ab1b818
Provides: bundled(golang(github.com/ghodss/yaml)) = 73d445a93680fa1a78ae23a5839bad48f32ba1ee
Provides: bundled(golang(github.com/go-ini/ini)) = 2e44421e256d82ebbf3d4d4fcabe8930b905eff3
Provides: bundled(golang(github.com/go-openapi/analysis)) = b44dc874b601d9e4e2f6e19140e794ba24bead3b
Provides: bundled(golang(github.com/go-openapi/errors)) = d24ebc2075bad502fac3a8ae27aa6dd58e1952dc
Provides: bundled(golang(github.com/go-openapi/jsonpointer)) = 46af16f9f7b149af66e5d1bd010e3574dc06de98
Provides: bundled(golang(github.com/go-openapi/jsonreference)) = 13c6e3589ad90f49bd3e3bbe2c2cb3d7a4142272
Provides: bundled(golang(github.com/go-openapi/loads)) = 18441dfa706d924a39a030ee2c3b1d8d81917b38
Provides: bundled(golang(github.com/go-openapi/loads/fmts)) = 18441dfa706d924a39a030ee2c3b1d8d81917b38
Provides: bundled(golang(github.com/go-openapi/runtime)) = 11e322eeecc1032d5a0a96c566ed53f2b5c26e22
Provides: bundled(golang(github.com/go-openapi/spec)) = 6aced65f8501fe1217321abf0749d354824ba2ff
Provides: bundled(golang(github.com/go-openapi/strfmt)) = d65c7fdb29eca313476e529628176fe17e58c488
Provides: bundled(golang(github.com/go-openapi/swag)) = 1d0bd113de87027671077d3c71eb3ac5d7dbba72
Provides: bundled(golang(github.com/go-openapi/validate)) = deaf2c9013bc1a7f4c774662259a506ba874d80f
Provides: bundled(golang(github.com/godbus/dbus)) = c7fdd8b5cd55e87b4e1f4e372cdb1db61dd6c66f
Provides: bundled(golang(github.com/gogo/protobuf/gogoproto)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/plugin/compare)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/plugin/defaultcheck)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/plugin/description)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/plugin/embedcheck)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/plugin/enumstringer)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/plugin/equal)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/plugin/face)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/plugin/gostring)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/plugin/marshalto)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/plugin/oneofcheck)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/plugin/populate)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/plugin/size)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/plugin/stringer)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/plugin/testgen)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/plugin/union)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/plugin/unmarshal)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/proto)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/protoc-gen-gogo/descriptor)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/protoc-gen-gogo/generator)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/protoc-gen-gogo/grpc)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/protoc-gen-gogo/plugin)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/sortkeys)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/vanity)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/gogo/protobuf/vanity/command)) = e18d7aa8f8c624c915db340349aad4c49b10d173
Provides: bundled(golang(github.com/golang/glog)) = 44145f04b68cf362d9c4df2182967c2275eaefed
Provides: bundled(golang(github.com/golang/groupcache/lru)) = 02826c3e79038b59d737d3b1c0a1d937f71a4433
Provides: bundled(golang(github.com/golang/mock/gomock)) = bd3c8e81be01eef76d4b503f5e687d2d1354d2d9
Provides: bundled(golang(github.com/golang/mock/gomock/mock_matcher)) = bd3c8e81be01eef76d4b503f5e687d2d1354d2d9
Provides: bundled(golang(github.com/golang/protobuf/jsonpb)) = 8616e8ee5e20a1704615e6c8d7afcdac06087a67
Provides: bundled(golang(github.com/golang/protobuf/proto)) = 8616e8ee5e20a1704615e6c8d7afcdac06087a67
Provides: bundled(golang(github.com/gonum/blas)) = 80dca99229cccca259b550ae3f755cf79c65a224
Provides: bundled(golang(github.com/gonum/blas/blas64)) = 80dca99229cccca259b550ae3f755cf79c65a224
Provides: bundled(golang(github.com/gonum/blas/native)) = 80dca99229cccca259b550ae3f755cf79c65a224
Provides: bundled(golang(github.com/gonum/blas/native/internal/math32)) = 80dca99229cccca259b550ae3f755cf79c65a224
Provides: bundled(golang(github.com/gonum/graph)) = bde6d0fbd9dec5a997e906611fe0364001364c41
Provides: bundled(golang(github.com/gonum/graph/concrete)) = bde6d0fbd9dec5a997e906611fe0364001364c41
Provides: bundled(golang(github.com/gonum/graph/encoding/dot)) = bde6d0fbd9dec5a997e906611fe0364001364c41
Provides: bundled(golang(github.com/gonum/graph/internal)) = bde6d0fbd9dec5a997e906611fe0364001364c41
Provides: bundled(golang(github.com/gonum/graph/path)) = bde6d0fbd9dec5a997e906611fe0364001364c41
Provides: bundled(golang(github.com/gonum/graph/topo)) = bde6d0fbd9dec5a997e906611fe0364001364c41
Provides: bundled(golang(github.com/gonum/graph/traverse)) = bde6d0fbd9dec5a997e906611fe0364001364c41
Provides: bundled(golang(github.com/gonum/internal/asm)) = 5b84ddfb9d3e72d73b8de858c97650be140935c0
Provides: bundled(golang(github.com/gonum/lapack)) = 88ec467285859a6cd23900147d250a8af1f38b10
Provides: bundled(golang(github.com/gonum/lapack/lapack64)) = 88ec467285859a6cd23900147d250a8af1f38b10
Provides: bundled(golang(github.com/gonum/lapack/native)) = 88ec467285859a6cd23900147d250a8af1f38b10
Provides: bundled(golang(github.com/gonum/matrix/mat64)) = fb1396264e2e259ff714a408a7b0142d238b198d
Provides: bundled(golang(github.com/google/btree)) = 7d79101e329e5a3adf994758c578dab82b90c017
Provides: bundled(golang(github.com/google/cadvisor/api)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/cache/memory)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/client/v2)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/collector)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/container)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/container/common)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/container/docker)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/container/libcontainer)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/container/raw)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/container/rkt)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/container/systemd)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/devicemapper)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/events)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/fs)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/healthz)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/http)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/http/mux)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/info/v1)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/info/v2)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/machine)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/manager)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/manager/watcher)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/manager/watcher/raw)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/manager/watcher/rkt)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/metrics)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/pages)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/pages/static)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/storage)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/summary)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/utils)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/utils/cloudinfo)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/utils/cpuload)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/utils/cpuload/netlink)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/utils/docker)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/utils/oomparser)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/utils/sysfs)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/utils/sysinfo)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/utils/tail)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/validate)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/version)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/cadvisor/zfs)) = 2ddeb5f60e22d86c8d1eeb654dfb8bfadf93374c
Provides: bundled(golang(github.com/google/certificate-transparency/go)) = a85d8bf28a950826bf6bc0693caf384ab4c6bec9
Provides: bundled(golang(github.com/google/certificate-transparency/go/asn1)) = a85d8bf28a950826bf6bc0693caf384ab4c6bec9
Provides: bundled(golang(github.com/google/certificate-transparency/go/client)) = a85d8bf28a950826bf6bc0693caf384ab4c6bec9
Provides: bundled(golang(github.com/google/certificate-transparency/go/x509)) = a85d8bf28a950826bf6bc0693caf384ab4c6bec9
Provides: bundled(golang(github.com/google/certificate-transparency/go/x509/pkix)) = a85d8bf28a950826bf6bc0693caf384ab4c6bec9
Provides: bundled(golang(github.com/google/gofuzz)) = 44d81051d367757e1c7c6a5a86423ece9afcf63c
Provides: bundled(golang(github.com/gophercloud/gophercloud)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/blockstorage/v1/apiversions)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/blockstorage/v1/volumes)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/blockstorage/v2/volumes)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/common/extensions)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/volumeattach)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/compute/v2/flavors)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/compute/v2/images)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/compute/v2/servers)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/identity/v2/tenants)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/identity/v2/tokens)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/identity/v3/extensions/trusts)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/identity/v3/tokens)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/networking/v2/extensions)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/layer3/floatingips)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/layer3/routers)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/lbaas/members)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/lbaas/monitors)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/lbaas/pools)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/lbaas/vips)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/lbaas_v2/listeners)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/lbaas_v2/loadbalancers)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/lbaas_v2/monitors)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/lbaas_v2/pools)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/security/groups)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/networking/v2/extensions/security/rules)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/networking/v2/ports)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/openstack/utils)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gophercloud/gophercloud/pagination)) = b06120d13e262ceaf890ef38ee30898813696af0
Provides: bundled(golang(github.com/gorilla/context)) = 215affda49addc4c8ef7e2534915df2c8c35c6cd
Provides: bundled(golang(github.com/gorilla/handlers)) = 4ef72b2795a418935d497c8db213080be06f8850
Provides: bundled(golang(github.com/gorilla/mux)) = 8096f47503459bcc74d1f4c487b7e6e42e5746b5
Provides: bundled(golang(github.com/gorilla/securecookie)) = 1b0c7f6e9ab3d7f500fd7d50c7ad835ff428139b
Provides: bundled(golang(github.com/gorilla/sessions)) = aa5e036e6c44aec69a32eb41097001978b29ad31
Provides: bundled(golang(github.com/grpc-ecosystem/go-grpc-prometheus)) = 6b7015e65d366bf3f19b2b2a000a831940f0f7e0
Provides: bundled(golang(github.com/grpc-ecosystem/grpc-gateway/runtime)) = f52d055dc48aec25854ed7d31862f78913cf17d1
Provides: bundled(golang(github.com/grpc-ecosystem/grpc-gateway/runtime/internal)) = f52d055dc48aec25854ed7d31862f78913cf17d1
Provides: bundled(golang(github.com/grpc-ecosystem/grpc-gateway/utilities)) = f52d055dc48aec25854ed7d31862f78913cf17d1
Provides: bundled(golang(github.com/hashicorp/go-cleanhttp)) = 3573b8b52aa7b37b9358d966a898feb387f62437
Provides: bundled(golang(github.com/hashicorp/golang-lru)) = a0d98a5f288019575c6d1f4bb1573fef2d1fcdc4
Provides: bundled(golang(github.com/hashicorp/golang-lru/simplelru)) = a0d98a5f288019575c6d1f4bb1573fef2d1fcdc4
Provides: bundled(golang(github.com/hashicorp/hcl)) = d8c773c4cba11b11539e3d45f93daeaa5dcf1fa1
Provides: bundled(golang(github.com/hashicorp/hcl/hcl/ast)) = d8c773c4cba11b11539e3d45f93daeaa5dcf1fa1
Provides: bundled(golang(github.com/hashicorp/hcl/hcl/parser)) = d8c773c4cba11b11539e3d45f93daeaa5dcf1fa1
Provides: bundled(golang(github.com/hashicorp/hcl/hcl/scanner)) = d8c773c4cba11b11539e3d45f93daeaa5dcf1fa1
Provides: bundled(golang(github.com/hashicorp/hcl/hcl/strconv)) = d8c773c4cba11b11539e3d45f93daeaa5dcf1fa1
Provides: bundled(golang(github.com/hashicorp/hcl/hcl/token)) = d8c773c4cba11b11539e3d45f93daeaa5dcf1fa1
Provides: bundled(golang(github.com/hashicorp/hcl/json/parser)) = d8c773c4cba11b11539e3d45f93daeaa5dcf1fa1
Provides: bundled(golang(github.com/hashicorp/hcl/json/scanner)) = d8c773c4cba11b11539e3d45f93daeaa5dcf1fa1
Provides: bundled(golang(github.com/hashicorp/hcl/json/token)) = d8c773c4cba11b11539e3d45f93daeaa5dcf1fa1
Provides: bundled(golang(github.com/hawkular/hawkular-client-go/metrics)) = 1d46ce7e1eca635f372357a8ccbf1fa7cc28b7d2
Provides: bundled(golang(github.com/heketi/heketi/client/api/go-client)) = 28b5cc4cc6d2b9bdfa91ed1b93efaab4931aa697
Provides: bundled(golang(github.com/heketi/heketi/pkg/glusterfs/api)) = 28b5cc4cc6d2b9bdfa91ed1b93efaab4931aa697
Provides: bundled(golang(github.com/heketi/heketi/pkg/utils)) = 28b5cc4cc6d2b9bdfa91ed1b93efaab4931aa697
Provides: bundled(golang(github.com/howeyc/gopass)) = 3ca23474a7c7203e0a0a070fd33508f6efdb9b3d
Provides: bundled(golang(github.com/imdario/mergo)) = 6633656539c1639d9d78127b7d47c622b5d7b6dc
Provides: bundled(golang(github.com/inconshreveable/mousetrap)) = 76626ae9c91c4f2a10f34cad8ce83ea42c93bb75
Provides: bundled(golang(github.com/influxdata/influxdb/client)) = e47cf1f2e83a02443d7115c54f838be8ee959644
Provides: bundled(golang(github.com/influxdata/influxdb/client/v2)) = e47cf1f2e83a02443d7115c54f838be8ee959644
Provides: bundled(golang(github.com/influxdata/influxdb/models)) = e47cf1f2e83a02443d7115c54f838be8ee959644
Provides: bundled(golang(github.com/influxdata/influxdb/pkg/escape)) = e47cf1f2e83a02443d7115c54f838be8ee959644
Provides: bundled(golang(github.com/jmespath/go-jmespath)) = 3433f3ea46d9f8019119e7dd41274e112a2359a9
Provides: bundled(golang(github.com/joho/godotenv)) = 4ed13390c0acd2ff4e371e64d8b97c8954138243
Provides: bundled(golang(github.com/jonboulle/clockwork)) = 3f831b65b61282ba6bece21b91beea2edc4c887a
Provides: bundled(golang(github.com/jteeuwen/go-bindata)) = a0ff2567cfb70903282db057e799fd826784d41d
Provides: bundled(golang(github.com/jteeuwen/go-bindata/go-bindata)) = a0ff2567cfb70903282db057e799fd826784d41d
Provides: bundled(golang(github.com/juju/ratelimit)) = 5b9ff866471762aa2ab2dced63c9fb6f53921342
Provides: bundled(golang(github.com/kardianos/osext)) = 8fef92e41e22a70e700a96b29f066cda30ea24ef
Provides: bundled(golang(github.com/kr/fs)) = 2788f0dbd16903de03cb8186e5c7d97b69ad387b
Provides: bundled(golang(github.com/kr/pty)) = f7ee69f31298ecbe5d2b349c711e2547a617d398
Provides: bundled(golang(github.com/lestrrat/go-jspointer)) = f4881e611bdbe9fb413a7780721ef8400a1f2341
Provides: bundled(golang(github.com/lestrrat/go-jsref)) = 50df7b2d07d799426a9ac43fa24bdb4785f72a54
Provides: bundled(golang(github.com/lestrrat/go-jsref/provider)) = 50df7b2d07d799426a9ac43fa24bdb4785f72a54
Provides: bundled(golang(github.com/lestrrat/go-jsschema)) = a6a42341b50d8d7e2a733db922eefaa756321021
Provides: bundled(golang(github.com/lestrrat/go-pdebug)) = 2e6eaaa5717f81bda41d27070d3c966f40a1e75f
Provides: bundled(golang(github.com/lestrrat/go-structinfo)) = f74c056fe41f860aa6264478c664a6fff8a64298
Provides: bundled(golang(github.com/libopenstorage/openstorage/api)) = 6e787003b91ddba85f108b8aede075b1af0d3606
Provides: bundled(golang(github.com/libopenstorage/openstorage/api/client)) = 6e787003b91ddba85f108b8aede075b1af0d3606
Provides: bundled(golang(github.com/libopenstorage/openstorage/api/client/volume)) = 6e787003b91ddba85f108b8aede075b1af0d3606
Provides: bundled(golang(github.com/libopenstorage/openstorage/api/spec)) = 6e787003b91ddba85f108b8aede075b1af0d3606
Provides: bundled(golang(github.com/libopenstorage/openstorage/pkg/units)) = 6e787003b91ddba85f108b8aede075b1af0d3606
Provides: bundled(golang(github.com/libopenstorage/openstorage/volume)) = 6e787003b91ddba85f108b8aede075b1af0d3606
Provides: bundled(golang(github.com/lpabon/godbc)) = 9577782540c1398b710ddae1b86268ba03a19b0c
Provides: bundled(golang(github.com/magiconair/properties)) = 61b492c03cf472e0c6419be5899b8e0dc28b1b88
Provides: bundled(golang(github.com/mailru/easyjson/buffer)) = e978125a7e335d8f4db746a9ac5b44643f27416b
Provides: bundled(golang(github.com/mailru/easyjson/jlexer)) = e978125a7e335d8f4db746a9ac5b44643f27416b
Provides: bundled(golang(github.com/mailru/easyjson/jwriter)) = e978125a7e335d8f4db746a9ac5b44643f27416b
Provides: bundled(golang(github.com/matttproud/golang_protobuf_extensions/pbutil)) = fc2b8d3a73c4867e51861bbdd5ae3c1f0869dd6a
Provides: bundled(golang(github.com/mesos/mesos-go/detector)) = 45c8b08e9af666add36a6f93ff8c1c75812367b0
Provides: bundled(golang(github.com/mesos/mesos-go/detector/zoo)) = 45c8b08e9af666add36a6f93ff8c1c75812367b0
Provides: bundled(golang(github.com/mesos/mesos-go/mesosproto)) = 45c8b08e9af666add36a6f93ff8c1c75812367b0
Provides: bundled(golang(github.com/mesos/mesos-go/mesosutil)) = 45c8b08e9af666add36a6f93ff8c1c75812367b0
Provides: bundled(golang(github.com/mesos/mesos-go/upid)) = 45c8b08e9af666add36a6f93ff8c1c75812367b0
Provides: bundled(golang(github.com/miekg/coredns/middleware/etcd/msg)) = 20e25559d5eada5a68a0720816a6e947b94860ce
Provides: bundled(golang(github.com/miekg/dns)) = 5d001d020961ae1c184f9f8152fdc73810481677
Provides: bundled(golang(github.com/mistifyio/go-zfs)) = 1b4ae6fb4e77b095934d4430860ff202060169f8
Provides: bundled(golang(github.com/mitchellh/go-wordwrap)) = ad45545899c7b13c020ea92b2072220eefad42b8
Provides: bundled(golang(github.com/mitchellh/mapstructure)) = 740c764bc6149d3f1806231418adb9f52c11bcbf
Provides: bundled(golang(github.com/mreiferson/go-httpclient)) = 31f0106b4474f14bc441575c19d3a5fa21aa1f6c
Provides: bundled(golang(github.com/mtrmac/gpgme)) = b2432428689ca58c2b8e8dea9449d3295cf96fc9
Provides: bundled(golang(github.com/mvdan/xurls)) = 1b768d7c393abd8e8dda1458385a57becd4b2d4e
Provides: bundled(golang(github.com/mxk/go-flowrate/flowrate)) = cca7078d478f8520f85629ad7c68962d31ed7682
Provides: bundled(golang(github.com/ncw/swift)) = c54732e87b0b283d1baf0a18db689d0aea460ba3
Provides: bundled(golang(github.com/onsi/ginkgo)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/config)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/ginkgo)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/ginkgo/convert)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/ginkgo/interrupthandler)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/ginkgo/nodot)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/ginkgo/testrunner)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/ginkgo/testsuite)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/ginkgo/watch)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/internal/codelocation)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/internal/containernode)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/internal/failer)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/internal/leafnodes)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/internal/remote)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/internal/spec)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/internal/spec_iterator)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/internal/specrunner)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/internal/suite)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/internal/testingtproxy)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/internal/writer)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/reporters)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/reporters/stenographer)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/reporters/stenographer/support/go-colorable)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/reporters/stenographer/support/go-isatty)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/ginkgo/types)) = 67b9df7f55fe1165fd9ad49aca7754cce01a42b8
Provides: bundled(golang(github.com/onsi/gomega)) = d59fa0ac68bb5dd932ee8d24eed631cdd519efc3
Provides: bundled(golang(github.com/onsi/gomega/format)) = d59fa0ac68bb5dd932ee8d24eed631cdd519efc3
Provides: bundled(golang(github.com/onsi/gomega/gbytes)) = d59fa0ac68bb5dd932ee8d24eed631cdd519efc3
Provides: bundled(golang(github.com/onsi/gomega/gexec)) = d59fa0ac68bb5dd932ee8d24eed631cdd519efc3
Provides: bundled(golang(github.com/onsi/gomega/gstruct)) = d59fa0ac68bb5dd932ee8d24eed631cdd519efc3
Provides: bundled(golang(github.com/onsi/gomega/gstruct/errors)) = d59fa0ac68bb5dd932ee8d24eed631cdd519efc3
Provides: bundled(golang(github.com/onsi/gomega/internal/assertion)) = d59fa0ac68bb5dd932ee8d24eed631cdd519efc3
Provides: bundled(golang(github.com/onsi/gomega/internal/asyncassertion)) = d59fa0ac68bb5dd932ee8d24eed631cdd519efc3
Provides: bundled(golang(github.com/onsi/gomega/internal/oraclematcher)) = d59fa0ac68bb5dd932ee8d24eed631cdd519efc3
Provides: bundled(golang(github.com/onsi/gomega/internal/testingtsupport)) = d59fa0ac68bb5dd932ee8d24eed631cdd519efc3
Provides: bundled(golang(github.com/onsi/gomega/matchers)) = d59fa0ac68bb5dd932ee8d24eed631cdd519efc3
Provides: bundled(golang(github.com/onsi/gomega/matchers/support/goraph/bipartitegraph)) = d59fa0ac68bb5dd932ee8d24eed631cdd519efc3
Provides: bundled(golang(github.com/onsi/gomega/matchers/support/goraph/edge)) = d59fa0ac68bb5dd932ee8d24eed631cdd519efc3
Provides: bundled(golang(github.com/onsi/gomega/matchers/support/goraph/node)) = d59fa0ac68bb5dd932ee8d24eed631cdd519efc3
Provides: bundled(golang(github.com/onsi/gomega/matchers/support/goraph/util)) = d59fa0ac68bb5dd932ee8d24eed631cdd519efc3
Provides: bundled(golang(github.com/onsi/gomega/types)) = d59fa0ac68bb5dd932ee8d24eed631cdd519efc3
Provides: bundled(golang(github.com/opencontainers/go-digest)) = a6d0ee40d4207ea02364bd3b9e8e77b9159ba1eb
Provides: bundled(golang(github.com/opencontainers/image-spec/specs-go)) = 00850eca2ab993e282a4921f8b7000b2fcbd26fa
Provides: bundled(golang(github.com/opencontainers/image-spec/specs-go/v1)) = 00850eca2ab993e282a4921f8b7000b2fcbd26fa
Provides: bundled(golang(github.com/opencontainers/runc/libcontainer)) = d223e2adae83f62d58448a799a5da05730228089
Provides: bundled(golang(github.com/opencontainers/runc/libcontainer/apparmor)) = d223e2adae83f62d58448a799a5da05730228089
Provides: bundled(golang(github.com/opencontainers/runc/libcontainer/cgroups)) = d223e2adae83f62d58448a799a5da05730228089
Provides: bundled(golang(github.com/opencontainers/runc/libcontainer/cgroups/fs)) = d223e2adae83f62d58448a799a5da05730228089
Provides: bundled(golang(github.com/opencontainers/runc/libcontainer/cgroups/systemd)) = d223e2adae83f62d58448a799a5da05730228089
Provides: bundled(golang(github.com/opencontainers/runc/libcontainer/configs)) = d223e2adae83f62d58448a799a5da05730228089
Provides: bundled(golang(github.com/opencontainers/runc/libcontainer/configs/validate)) = d223e2adae83f62d58448a799a5da05730228089
Provides: bundled(golang(github.com/opencontainers/runc/libcontainer/criurpc)) = d223e2adae83f62d58448a799a5da05730228089
Provides: bundled(golang(github.com/opencontainers/runc/libcontainer/keys)) = d223e2adae83f62d58448a799a5da05730228089
Provides: bundled(golang(github.com/opencontainers/runc/libcontainer/label)) = d223e2adae83f62d58448a799a5da05730228089
Provides: bundled(golang(github.com/opencontainers/runc/libcontainer/seccomp)) = d223e2adae83f62d58448a799a5da05730228089
Provides: bundled(golang(github.com/opencontainers/runc/libcontainer/selinux)) = d223e2adae83f62d58448a799a5da05730228089
Provides: bundled(golang(github.com/opencontainers/runc/libcontainer/stacktrace)) = d223e2adae83f62d58448a799a5da05730228089
Provides: bundled(golang(github.com/opencontainers/runc/libcontainer/system)) = d223e2adae83f62d58448a799a5da05730228089
Provides: bundled(golang(github.com/opencontainers/runc/libcontainer/user)) = d223e2adae83f62d58448a799a5da05730228089
Provides: bundled(golang(github.com/opencontainers/runc/libcontainer/utils)) = d223e2adae83f62d58448a799a5da05730228089
Provides: bundled(golang(github.com/openshift/imagebuilder)) = 1c70938feddeb3ef9368091726e3c8a662dd7ac5
Provides: bundled(golang(github.com/openshift/imagebuilder/dockerclient)) = 1c70938feddeb3ef9368091726e3c8a662dd7ac5
Provides: bundled(golang(github.com/openshift/imagebuilder/imageprogress)) = 1c70938feddeb3ef9368091726e3c8a662dd7ac5
Provides: bundled(golang(github.com/openshift/imagebuilder/signal)) = 1c70938feddeb3ef9368091726e3c8a662dd7ac5
Provides: bundled(golang(github.com/openshift/imagebuilder/strslice)) = 1c70938feddeb3ef9368091726e3c8a662dd7ac5
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/api)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/api/describe)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/api/validation)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/build)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/build/strategies)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/build/strategies/layered)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/build/strategies/onbuild)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/build/strategies/sti)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/docker)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/errors)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/ignore)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/scm)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/scm/empty)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/scm/file)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/scm/git)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/scripts)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/tar)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/test)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/util)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/util/glog)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/util/interrupt)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/util/status)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/openshift/source-to-image/pkg/util/user)) = 7f58756254b0a65bf59fa87a8ecedad01ce6a85b
Provides: bundled(golang(github.com/pborman/uuid)) = ca53cad383cad2479bbba7f7a1a05797ec1386e4
Provides: bundled(golang(github.com/pelletier/go-buffruneio)) = df1e16fde7fc330a0ca68167c23bf7ed6ac31d6d
Provides: bundled(golang(github.com/pelletier/go-toml)) = 0049ab3dc4c4c70a9eee23087437b69c0dde2130
Provides: bundled(golang(github.com/pkg/errors)) = a22138067af1c4942683050411a841ade67fe1eb
Provides: bundled(golang(github.com/pkg/profile)) = c795610ec6e479e5795f7852db65ea15073674a6
Provides: bundled(golang(github.com/pkg/sftp)) = 4d0e916071f68db74f8a73926335f809396d6b42
Provides: bundled(golang(github.com/pmezard/go-difflib/difflib)) = d8ed2627bdf02c080bf22230dbb337003b7aba2d
Provides: bundled(golang(github.com/prometheus/client_golang/prometheus)) = e51041b3fa41cece0dca035740ba6411905be473
Provides: bundled(golang(github.com/prometheus/client_model/go)) = fa8ad6fec33561be4280a8f0514318c79d7f6cb6
Provides: bundled(golang(github.com/prometheus/common/expfmt)) = a6ab08426bb262e2d190097751f5cfd1cfdfd17d
Provides: bundled(golang(github.com/prometheus/common/internal/bitbucket.org/ww/goautoneg)) = a6ab08426bb262e2d190097751f5cfd1cfdfd17d
Provides: bundled(golang(github.com/prometheus/common/model)) = a6ab08426bb262e2d190097751f5cfd1cfdfd17d
Provides: bundled(golang(github.com/prometheus/procfs)) = 454a56f35412459b5e684fd5ec0f9211b94f002a
Provides: bundled(golang(github.com/quobyte/api)) = bf713b5a4333f44504fa1ce63690de45cfed6413
Provides: bundled(golang(github.com/rackspace/gophercloud)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/openstack)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/openstack/blockstorage/v1/volumes)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/openstack/compute/v2/extensions/bootfromvolume)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/openstack/compute/v2/extensions/diskconfig)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/openstack/compute/v2/extensions/volumeattach)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/openstack/compute/v2/flavors)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/openstack/compute/v2/images)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/openstack/compute/v2/servers)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/openstack/identity/v2/tenants)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/openstack/identity/v2/tokens)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/openstack/identity/v3/tokens)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/openstack/utils)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/pagination)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/rackspace)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/rackspace/blockstorage/v1/volumes)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/rackspace/compute/v2/servers)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/rackspace/compute/v2/volumeattach)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/rackspace/identity/v2/tokens)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/testhelper)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/rackspace/gophercloud/testhelper/client)) = e00690e87603abe613e9f02c816c7c4bef82e063
Provides: bundled(golang(github.com/renstrom/dedent)) = 020d11c3b9c0c7a3c2efcc8e5cf5b9ef7bcea21f
Provides: bundled(golang(github.com/robfig/cron)) = 783cfcb01fb00c48f740c9de79122bd410294149
Provides: bundled(golang(github.com/rubiojr/go-vhd/vhd)) = 0bfd3b39853cdde5762efda92289f14b0ac0491b
Provides: bundled(golang(github.com/russross/blackfriday)) = 300106c228d52c8941d4b3de6054a6062a86dda3
Provides: bundled(golang(github.com/samuel/go-zookeeper/zk)) = 177002e16a0061912f02377e2dd8951a8b3551bc
Provides: bundled(golang(github.com/seccomp/libseccomp-golang)) = 1b506fc7c24eec5a3693cdcbed40d9c226cfc6a1
Provides: bundled(golang(github.com/shurcooL/sanitized_anchor_name)) = 10ef21a441db47d8b13ebcc5fd2310f636973c77
Provides: bundled(golang(github.com/skynetservices/skydns/cache)) = 30763c4e568fe411f1663af553c063cec8879929
Provides: bundled(golang(github.com/skynetservices/skydns/metrics)) = 30763c4e568fe411f1663af553c063cec8879929
Provides: bundled(golang(github.com/skynetservices/skydns/msg)) = 30763c4e568fe411f1663af553c063cec8879929
Provides: bundled(golang(github.com/skynetservices/skydns/server)) = 30763c4e568fe411f1663af553c063cec8879929
Provides: bundled(golang(github.com/skynetservices/skydns/singleflight)) = 30763c4e568fe411f1663af553c063cec8879929
Provides: bundled(golang(github.com/spf13/afero)) = b28a7effac979219c2a2ed6205a4d70e4b1bcd02
Provides: bundled(golang(github.com/spf13/afero/mem)) = b28a7effac979219c2a2ed6205a4d70e4b1bcd02
Provides: bundled(golang(github.com/spf13/afero/sftp)) = b28a7effac979219c2a2ed6205a4d70e4b1bcd02
Provides: bundled(golang(github.com/spf13/cast)) = e31f36ffc91a2ba9ddb72a4b6a607ff9b3d3cb63
Provides: bundled(golang(github.com/spf13/cobra)) = 7c674d9e72017ed25f6d2b5e497a1368086b6a6f
Provides: bundled(golang(github.com/spf13/cobra/doc)) = 7c674d9e72017ed25f6d2b5e497a1368086b6a6f
Provides: bundled(golang(github.com/spf13/jwalterweatherman)) = 33c24e77fb80341fe7130ee7c594256ff08ccc46
Provides: bundled(golang(github.com/spf13/pflag)) = 9ff6c6923cfffbcd502984b8e0c80539a94968b7
Provides: bundled(golang(github.com/spf13/viper)) = 7fb2782df3d83e0036cc89f461ed0422628776f4
Provides: bundled(golang(github.com/square/go-jose)) = 789a4c4bd4c118f7564954f441b29c153ccd6a96
Provides: bundled(golang(github.com/square/go-jose/cipher)) = 789a4c4bd4c118f7564954f441b29c153ccd6a96
Provides: bundled(golang(github.com/square/go-jose/json)) = 789a4c4bd4c118f7564954f441b29c153ccd6a96
Provides: bundled(golang(github.com/stevvooe/resumable)) = 51ad44105773cafcbe91927f70ac68e1bf78f8b4
Provides: bundled(golang(github.com/stevvooe/resumable/sha256)) = 51ad44105773cafcbe91927f70ac68e1bf78f8b4
Provides: bundled(golang(github.com/stevvooe/resumable/sha512)) = 51ad44105773cafcbe91927f70ac68e1bf78f8b4
Provides: bundled(golang(github.com/stretchr/objx)) = 1a9d0bb9f541897e62256577b352fdbc1fb4fd94
Provides: bundled(golang(github.com/stretchr/testify/assert)) = e3a8ff8ce36581f87a15341206f205b1da467059
Provides: bundled(golang(github.com/stretchr/testify/mock)) = e3a8ff8ce36581f87a15341206f205b1da467059
Provides: bundled(golang(github.com/stretchr/testify/require)) = e3a8ff8ce36581f87a15341206f205b1da467059
Provides: bundled(golang(github.com/syndtr/gocapability/capability)) = e7cb7fa329f456b3855136a2642b197bad7366ba
Provides: bundled(golang(github.com/ugorji/go/codec)) = ded73eae5db7e7a0ef6f55aace87a2873c5d2b74
Provides: bundled(golang(github.com/vishvananda/netlink)) = 1e2e08e8a2dcdacaae3f14ac44c5cfa31361f270
Provides: bundled(golang(github.com/vishvananda/netlink/nl)) = 1e2e08e8a2dcdacaae3f14ac44c5cfa31361f270
Provides: bundled(golang(github.com/vishvananda/netns)) = 8ba1072b58e0c2a240eb5f6120165c7776c3e7b8
Provides: bundled(golang(github.com/vjeantet/asn1-ber)) = 85041cd0f4769ebf4a5ae600b1e921e630d6aff0
Provides: bundled(golang(github.com/vjeantet/ldapserver)) = 19fbc46ed12348d5122812c8303fb82e49b6c25d
Provides: bundled(golang(github.com/vmware/govmomi)) = 0a28e595c8e9e99879e8d2f796e82c5a68202ff0
Provides: bundled(golang(github.com/vmware/govmomi/find)) = 0a28e595c8e9e99879e8d2f796e82c5a68202ff0
Provides: bundled(golang(github.com/vmware/govmomi/list)) = 0a28e595c8e9e99879e8d2f796e82c5a68202ff0
Provides: bundled(golang(github.com/vmware/govmomi/object)) = 0a28e595c8e9e99879e8d2f796e82c5a68202ff0
Provides: bundled(golang(github.com/vmware/govmomi/property)) = 0a28e595c8e9e99879e8d2f796e82c5a68202ff0
Provides: bundled(golang(github.com/vmware/govmomi/session)) = 0a28e595c8e9e99879e8d2f796e82c5a68202ff0
Provides: bundled(golang(github.com/vmware/govmomi/task)) = 0a28e595c8e9e99879e8d2f796e82c5a68202ff0
Provides: bundled(golang(github.com/vmware/govmomi/vim25)) = 0a28e595c8e9e99879e8d2f796e82c5a68202ff0
Provides: bundled(golang(github.com/vmware/govmomi/vim25/debug)) = 0a28e595c8e9e99879e8d2f796e82c5a68202ff0
Provides: bundled(golang(github.com/vmware/govmomi/vim25/methods)) = 0a28e595c8e9e99879e8d2f796e82c5a68202ff0
Provides: bundled(golang(github.com/vmware/govmomi/vim25/mo)) = 0a28e595c8e9e99879e8d2f796e82c5a68202ff0
Provides: bundled(golang(github.com/vmware/govmomi/vim25/progress)) = 0a28e595c8e9e99879e8d2f796e82c5a68202ff0
Provides: bundled(golang(github.com/vmware/govmomi/vim25/soap)) = 0a28e595c8e9e99879e8d2f796e82c5a68202ff0
Provides: bundled(golang(github.com/vmware/govmomi/vim25/types)) = 0a28e595c8e9e99879e8d2f796e82c5a68202ff0
Provides: bundled(golang(github.com/vmware/govmomi/vim25/xml)) = 0a28e595c8e9e99879e8d2f796e82c5a68202ff0
Provides: bundled(golang(github.com/vmware/photon-controller-go-sdk/SSPI)) = 4a435daef6ccd3d0edaac1161e76f51a70c2589a
Provides: bundled(golang(github.com/vmware/photon-controller-go-sdk/photon)) = 4a435daef6ccd3d0edaac1161e76f51a70c2589a
Provides: bundled(golang(github.com/vmware/photon-controller-go-sdk/photon/lightwave)) = 4a435daef6ccd3d0edaac1161e76f51a70c2589a
Provides: bundled(golang(github.com/xanzy/go-cloudstack/cloudstack)) = 1e2cbf647e57fa90353612074fdfc42faf5073bf
Provides: bundled(golang(github.com/xiang90/probing)) = 07dd2e8dfe18522e9c447ba95f2fe95262f63bb2
Provides: bundled(golang(github.com/xyproto/simpleredis)) = 5292687f5379e01054407da44d7c4590a61fd3de
Provides: bundled(golang(go.pedge.io/pb/go/google/protobuf)) = f3c84f58974dc53d460d0855337cad85843bf0df
Provides: bundled(golang(go4.org/errorutil)) = 03efcb870d84809319ea509714dd6d19a1498483
Provides: bundled(golang(golang.org/x/crypto/bcrypt)) = d172538b2cfce0c13cee31e647d0367aa8cd2486
Provides: bundled(golang(golang.org/x/crypto/blowfish)) = d172538b2cfce0c13cee31e647d0367aa8cd2486
Provides: bundled(golang(golang.org/x/crypto/cast5)) = d172538b2cfce0c13cee31e647d0367aa8cd2486
Provides: bundled(golang(golang.org/x/crypto/curve25519)) = d172538b2cfce0c13cee31e647d0367aa8cd2486
Provides: bundled(golang(golang.org/x/crypto/ed25519)) = d172538b2cfce0c13cee31e647d0367aa8cd2486
Provides: bundled(golang(golang.org/x/crypto/ed25519/internal/edwards25519)) = d172538b2cfce0c13cee31e647d0367aa8cd2486
Provides: bundled(golang(golang.org/x/crypto/nacl/secretbox)) = d172538b2cfce0c13cee31e647d0367aa8cd2486
Provides: bundled(golang(golang.org/x/crypto/openpgp)) = d172538b2cfce0c13cee31e647d0367aa8cd2486
Provides: bundled(golang(golang.org/x/crypto/openpgp/armor)) = d172538b2cfce0c13cee31e647d0367aa8cd2486
Provides: bundled(golang(golang.org/x/crypto/openpgp/elgamal)) = d172538b2cfce0c13cee31e647d0367aa8cd2486
Provides: bundled(golang(golang.org/x/crypto/openpgp/errors)) = d172538b2cfce0c13cee31e647d0367aa8cd2486
Provides: bundled(golang(golang.org/x/crypto/openpgp/packet)) = d172538b2cfce0c13cee31e647d0367aa8cd2486
Provides: bundled(golang(golang.org/x/crypto/openpgp/s2k)) = d172538b2cfce0c13cee31e647d0367aa8cd2486
Provides: bundled(golang(golang.org/x/crypto/pkcs12)) = d172538b2cfce0c13cee31e647d0367aa8cd2486
Provides: bundled(golang(golang.org/x/crypto/pkcs12/internal/rc2)) = d172538b2cfce0c13cee31e647d0367aa8cd2486
Provides: bundled(golang(golang.org/x/crypto/poly1305)) = d172538b2cfce0c13cee31e647d0367aa8cd2486
Provides: bundled(golang(golang.org/x/crypto/salsa20/salsa)) = d172538b2cfce0c13cee31e647d0367aa8cd2486
Provides: bundled(golang(golang.org/x/crypto/ssh)) = d172538b2cfce0c13cee31e647d0367aa8cd2486
Provides: bundled(golang(golang.org/x/crypto/ssh/terminal)) = d172538b2cfce0c13cee31e647d0367aa8cd2486
Provides: bundled(golang(golang.org/x/exp/inotify)) = 292a51b8d262487dab23a588950e8052d63d9113
Provides: bundled(golang(golang.org/x/net/context)) = e90d6d0afc4c315a0d87a568ae68577cc15149a0
Provides: bundled(golang(golang.org/x/net/context/ctxhttp)) = e90d6d0afc4c315a0d87a568ae68577cc15149a0
Provides: bundled(golang(golang.org/x/net/html)) = e90d6d0afc4c315a0d87a568ae68577cc15149a0
Provides: bundled(golang(golang.org/x/net/html/atom)) = e90d6d0afc4c315a0d87a568ae68577cc15149a0
Provides: bundled(golang(golang.org/x/net/http2)) = e90d6d0afc4c315a0d87a568ae68577cc15149a0
Provides: bundled(golang(golang.org/x/net/http2/hpack)) = e90d6d0afc4c315a0d87a568ae68577cc15149a0
Provides: bundled(golang(golang.org/x/net/idna)) = e90d6d0afc4c315a0d87a568ae68577cc15149a0
Provides: bundled(golang(golang.org/x/net/internal/timeseries)) = e90d6d0afc4c315a0d87a568ae68577cc15149a0
Provides: bundled(golang(golang.org/x/net/lex/httplex)) = e90d6d0afc4c315a0d87a568ae68577cc15149a0
Provides: bundled(golang(golang.org/x/net/proxy)) = e90d6d0afc4c315a0d87a568ae68577cc15149a0
Provides: bundled(golang(golang.org/x/net/trace)) = e90d6d0afc4c315a0d87a568ae68577cc15149a0
Provides: bundled(golang(golang.org/x/net/websocket)) = e90d6d0afc4c315a0d87a568ae68577cc15149a0
Provides: bundled(golang(golang.org/x/oauth2)) = 3c3a985cb79f52a3190fbc056984415ca6763d01
Provides: bundled(golang(golang.org/x/oauth2/google)) = 3c3a985cb79f52a3190fbc056984415ca6763d01
Provides: bundled(golang(golang.org/x/oauth2/internal)) = 3c3a985cb79f52a3190fbc056984415ca6763d01
Provides: bundled(golang(golang.org/x/oauth2/jws)) = 3c3a985cb79f52a3190fbc056984415ca6763d01
Provides: bundled(golang(golang.org/x/oauth2/jwt)) = 3c3a985cb79f52a3190fbc056984415ca6763d01
Provides: bundled(golang(golang.org/x/sys/unix)) = 8d1157a435470616f975ff9bb013bea8d0962067
Provides: bundled(golang(golang.org/x/sys/windows)) = 8d1157a435470616f975ff9bb013bea8d0962067
Provides: bundled(golang(golang.org/x/text/cases)) = ceefd2213ed29504fff30155163c8f59827734f3
Provides: bundled(golang(golang.org/x/text/encoding)) = ceefd2213ed29504fff30155163c8f59827734f3
Provides: bundled(golang(golang.org/x/text/encoding/internal)) = ceefd2213ed29504fff30155163c8f59827734f3
Provides: bundled(golang(golang.org/x/text/encoding/internal/identifier)) = ceefd2213ed29504fff30155163c8f59827734f3
Provides: bundled(golang(golang.org/x/text/encoding/unicode)) = ceefd2213ed29504fff30155163c8f59827734f3
Provides: bundled(golang(golang.org/x/text/internal/tag)) = ceefd2213ed29504fff30155163c8f59827734f3
Provides: bundled(golang(golang.org/x/text/internal/utf8internal)) = ceefd2213ed29504fff30155163c8f59827734f3
Provides: bundled(golang(golang.org/x/text/language)) = ceefd2213ed29504fff30155163c8f59827734f3
Provides: bundled(golang(golang.org/x/text/runes)) = ceefd2213ed29504fff30155163c8f59827734f3
Provides: bundled(golang(golang.org/x/text/secure/bidirule)) = ceefd2213ed29504fff30155163c8f59827734f3
Provides: bundled(golang(golang.org/x/text/secure/precis)) = ceefd2213ed29504fff30155163c8f59827734f3
Provides: bundled(golang(golang.org/x/text/transform)) = ceefd2213ed29504fff30155163c8f59827734f3
Provides: bundled(golang(golang.org/x/text/unicode/bidi)) = ceefd2213ed29504fff30155163c8f59827734f3
Provides: bundled(golang(golang.org/x/text/unicode/norm)) = ceefd2213ed29504fff30155163c8f59827734f3
Provides: bundled(golang(golang.org/x/text/width)) = ceefd2213ed29504fff30155163c8f59827734f3
Provides: bundled(golang(google.golang.org/api/cloudmonitoring/v2beta2)) = 55146ba61254fdb1c26d65ff3c04bc1611ad73fb
Provides: bundled(golang(google.golang.org/api/compute/v1)) = 55146ba61254fdb1c26d65ff3c04bc1611ad73fb
Provides: bundled(golang(google.golang.org/api/container/v1)) = 55146ba61254fdb1c26d65ff3c04bc1611ad73fb
Provides: bundled(golang(google.golang.org/api/dns/v1)) = 55146ba61254fdb1c26d65ff3c04bc1611ad73fb
Provides: bundled(golang(google.golang.org/api/gensupport)) = 55146ba61254fdb1c26d65ff3c04bc1611ad73fb
Provides: bundled(golang(google.golang.org/api/googleapi)) = 55146ba61254fdb1c26d65ff3c04bc1611ad73fb
Provides: bundled(golang(google.golang.org/api/googleapi/internal/uritemplates)) = 55146ba61254fdb1c26d65ff3c04bc1611ad73fb
Provides: bundled(golang(google.golang.org/api/logging/v2beta1)) = 55146ba61254fdb1c26d65ff3c04bc1611ad73fb
Provides: bundled(golang(google.golang.org/api/storage/v1)) = 55146ba61254fdb1c26d65ff3c04bc1611ad73fb
Provides: bundled(golang(google.golang.org/appengine)) = 4f7eeb5305a4ba1966344836ba4af9996b7b4e05
Provides: bundled(golang(google.golang.org/appengine/internal)) = 4f7eeb5305a4ba1966344836ba4af9996b7b4e05
Provides: bundled(golang(google.golang.org/appengine/internal/app_identity)) = 4f7eeb5305a4ba1966344836ba4af9996b7b4e05
Provides: bundled(golang(google.golang.org/appengine/internal/base)) = 4f7eeb5305a4ba1966344836ba4af9996b7b4e05
Provides: bundled(golang(google.golang.org/appengine/internal/datastore)) = 4f7eeb5305a4ba1966344836ba4af9996b7b4e05
Provides: bundled(golang(google.golang.org/appengine/internal/log)) = 4f7eeb5305a4ba1966344836ba4af9996b7b4e05
Provides: bundled(golang(google.golang.org/appengine/internal/modules)) = 4f7eeb5305a4ba1966344836ba4af9996b7b4e05
Provides: bundled(golang(google.golang.org/appengine/internal/remote_api)) = 4f7eeb5305a4ba1966344836ba4af9996b7b4e05
Provides: bundled(golang(google.golang.org/cloud)) = eb47ba841d53d93506cfbfbc03927daf9cc48f88
Provides: bundled(golang(google.golang.org/cloud/internal)) = eb47ba841d53d93506cfbfbc03927daf9cc48f88
Provides: bundled(golang(google.golang.org/cloud/internal/opts)) = eb47ba841d53d93506cfbfbc03927daf9cc48f88
Provides: bundled(golang(google.golang.org/cloud/internal/transport)) = eb47ba841d53d93506cfbfbc03927daf9cc48f88
Provides: bundled(golang(google.golang.org/cloud/storage)) = eb47ba841d53d93506cfbfbc03927daf9cc48f88
Provides: bundled(golang(google.golang.org/grpc)) = b1a2821ca5a4fd6b6e48ddfbb7d6d7584d839d21
Provides: bundled(golang(google.golang.org/grpc/codes)) = b1a2821ca5a4fd6b6e48ddfbb7d6d7584d839d21
Provides: bundled(golang(google.golang.org/grpc/credentials)) = b1a2821ca5a4fd6b6e48ddfbb7d6d7584d839d21
Provides: bundled(golang(google.golang.org/grpc/credentials/oauth)) = b1a2821ca5a4fd6b6e48ddfbb7d6d7584d839d21
Provides: bundled(golang(google.golang.org/grpc/grpclog)) = b1a2821ca5a4fd6b6e48ddfbb7d6d7584d839d21
Provides: bundled(golang(google.golang.org/grpc/internal)) = b1a2821ca5a4fd6b6e48ddfbb7d6d7584d839d21
Provides: bundled(golang(google.golang.org/grpc/metadata)) = b1a2821ca5a4fd6b6e48ddfbb7d6d7584d839d21
Provides: bundled(golang(google.golang.org/grpc/naming)) = b1a2821ca5a4fd6b6e48ddfbb7d6d7584d839d21
Provides: bundled(golang(google.golang.org/grpc/peer)) = b1a2821ca5a4fd6b6e48ddfbb7d6d7584d839d21
Provides: bundled(golang(google.golang.org/grpc/transport)) = b1a2821ca5a4fd6b6e48ddfbb7d6d7584d839d21
Provides: bundled(golang(gopkg.in/asn1-ber.v1)) = 4e86f4367175e39f69d9358a5f17b4dda270378d
Provides: bundled(golang(gopkg.in/gcfg.v1)) = 083575c3955c85df16fe9590cceab64d03f5eb6e
Provides: bundled(golang(gopkg.in/gcfg.v1/scanner)) = 083575c3955c85df16fe9590cceab64d03f5eb6e
Provides: bundled(golang(gopkg.in/gcfg.v1/token)) = 083575c3955c85df16fe9590cceab64d03f5eb6e
Provides: bundled(golang(gopkg.in/gcfg.v1/types)) = 083575c3955c85df16fe9590cceab64d03f5eb6e
Provides: bundled(golang(gopkg.in/inf.v0)) = 3887ee99ecf07df5b447e9b00d9c0b2adaa9f3e4
Provides: bundled(golang(gopkg.in/ldap.v2)) = 8168ee085ee43257585e50c6441aadf54ecb2c9f
Provides: bundled(golang(gopkg.in/natefinch/lumberjack.v2)) = 20b71e5b60d756d3d2f80def009790325acc2b23
Provides: bundled(golang(gopkg.in/yaml.v2)) = a83829b6f1293c91addabc89d0571c246397bbf4
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/api/equality)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/api/errors)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/api/meta)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/api/resource)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/api/testing)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/api/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/api/validation/path)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/apimachinery)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/apimachinery/announced)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/apimachinery/registered)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/apis/meta/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/apis/meta/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/apis/meta/v1/unstructured)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/apis/meta/v1/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/conversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/conversion/queryparams)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/conversion/unstructured)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/fields)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/labels)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/openapi)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/runtime)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/runtime/schema)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/runtime/serializer)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/runtime/serializer/json)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/runtime/serializer/protobuf)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/runtime/serializer/recognizer)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/runtime/serializer/streaming)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/runtime/serializer/versioning)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/selection)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/types)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/diff)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/errors)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/framer)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/httpstream)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/httpstream/spdy)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/intstr)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/json)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/jsonmergepatch)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/mergepatch)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/net)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/rand)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/runtime)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/sets)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/strategicpatch)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/uuid)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/validation/field)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/wait)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/yaml)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/version)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/watch)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/third_party/forked/golang/json)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/third_party/forked/golang/netutil)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/third_party/forked/golang/reflect)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/admission)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/apis/apiserver)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/apis/apiserver/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/apis/apiserver/v1alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authentication/authenticator)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authentication/authenticatorfactory)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authentication/group)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authentication/request/anonymous)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authentication/request/bearertoken)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authentication/request/headerrequest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authentication/request/union)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authentication/request/x509)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authentication/serviceaccount)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authentication/token/tokenfile)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authentication/user)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authorization/authorizer)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authorization/authorizerfactory)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authorization/union)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/endpoints)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/endpoints/filters)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/endpoints/handlers)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/endpoints/handlers/negotiation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/endpoints/handlers/responsewriters)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/endpoints/metrics)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/endpoints/openapi)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/endpoints/request)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/features)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/registry/generic)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/registry/generic/registry)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/registry/generic/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/registry/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/registry/rest/resttest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/server)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/server/filters)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/server/healthz)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/server/httplog)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/server/mux)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/server/openapi)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/server/options)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/server/routes)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/server/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage/errors)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage/etcd)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage/etcd/etcdtest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage/etcd/metrics)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage/etcd/testing)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage/etcd/testing/testingcert)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage/etcd/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage/etcd3)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage/names)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage/storagebackend)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage/storagebackend/factory)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage/testing)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/util/cache)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/util/feature)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/util/flag)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/util/flushwriter)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/util/proxy)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/util/trace)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/util/trie)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/util/webhook)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/util/wsstream)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/plugin/pkg/authenticator/password/keystone)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/plugin/pkg/authenticator/password/passwordfile)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/plugin/pkg/authenticator/request/basicauth)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/plugin/pkg/authenticator/token/anytoken)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/plugin/pkg/authenticator/token/oidc)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/plugin/pkg/authenticator/token/webhook)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/plugin/pkg/authorizer/webhook)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/discovery)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/discovery/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/dynamic)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/scheme)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/apps/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/apps/v1beta1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/authentication/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/authentication/v1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/authentication/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/authentication/v1beta1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/authorization/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/authorization/v1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/authorization/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/authorization/v1beta1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/autoscaling/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/autoscaling/v1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/autoscaling/v2alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/autoscaling/v2alpha1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/batch/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/batch/v1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/batch/v2alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/batch/v2alpha1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/certificates/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/certificates/v1beta1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/core/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/core/v1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/extensions/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/extensions/v1beta1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/policy/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/policy/v1beta1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/rbac/v1alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/rbac/v1alpha1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/rbac/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/rbac/v1beta1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/settings/v1alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/settings/v1alpha1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/storage/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/storage/v1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/storage/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/storage/v1beta1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/api)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/api/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/api/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/apps)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/apps/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/apps/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/authentication)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/authentication/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/authentication/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/authentication/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/authorization)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/authorization/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/authorization/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/authorization/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/autoscaling)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/autoscaling/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/autoscaling/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/autoscaling/v2alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/batch)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/batch/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/batch/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/batch/v2alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/certificates)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/certificates/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/certificates/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/extensions)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/extensions/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/extensions/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/policy)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/policy/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/policy/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/rbac)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/rbac/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/rbac/v1alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/rbac/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/settings)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/settings/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/settings/v1alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/storage/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/storage/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/apis/storage/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/securitycontextconstraints/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/util/parsers)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/version)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/plugin/pkg/client/auth)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/plugin/pkg/client/auth/gcp)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/plugin/pkg/client/auth/oidc)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/rest/watch)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/testing)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/third_party/forked/golang/template)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/tools/auth)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/tools/cache)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/tools/clientcmd)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/tools/clientcmd/api)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/tools/clientcmd/api/latest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/tools/clientcmd/api/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/tools/metrics)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/tools/portforward)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/tools/record)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/transport)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/util/cert)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/util/clock)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/util/flowcontrol)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/util/homedir)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/util/integer)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/util/jsonpath)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/util/workqueue)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/gengo/args)) = c118aa8edfff53fe5b69127a970f54b6cf3a7563
Provides: bundled(golang(k8s.io/gengo/examples/deepcopy-gen/generators)) = c118aa8edfff53fe5b69127a970f54b6cf3a7563
Provides: bundled(golang(k8s.io/gengo/examples/defaulter-gen/generators)) = c118aa8edfff53fe5b69127a970f54b6cf3a7563
Provides: bundled(golang(k8s.io/gengo/examples/import-boss/generators)) = c118aa8edfff53fe5b69127a970f54b6cf3a7563
Provides: bundled(golang(k8s.io/gengo/examples/set-gen/generators)) = c118aa8edfff53fe5b69127a970f54b6cf3a7563
Provides: bundled(golang(k8s.io/gengo/examples/set-gen/sets)) = c118aa8edfff53fe5b69127a970f54b6cf3a7563
Provides: bundled(golang(k8s.io/gengo/generator)) = c118aa8edfff53fe5b69127a970f54b6cf3a7563
Provides: bundled(golang(k8s.io/gengo/namer)) = c118aa8edfff53fe5b69127a970f54b6cf3a7563
Provides: bundled(golang(k8s.io/gengo/parser)) = c118aa8edfff53fe5b69127a970f54b6cf3a7563
Provides: bundled(golang(k8s.io/gengo/types)) = c118aa8edfff53fe5b69127a970f54b6cf3a7563
Provides: bundled(golang(k8s.io/heapster/metrics/api/v1/types)) = c2ac40f1adf8c42a79badddb2a2acd673cae3bcb
Provides: bundled(golang(k8s.io/heapster/metrics/apis/metrics/v1alpha1)) = c2ac40f1adf8c42a79badddb2a2acd673cae3bcb
Provides: bundled(golang(k8s.io/kubernetes/cmd/genutils)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/kube-apiserver/app)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/kube-apiserver/app/options)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/kube-apiserver/app/preflight)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/kube-controller-manager/app)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/kube-controller-manager/app/options)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/kube-proxy/app)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/kube-proxy/app/options)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/kubeadm/app/apis/kubeadm)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/kubeadm/app/apis/kubeadm/fuzzer)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/kubelet/app)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/kubelet/app/options)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/libs/go2idl/client-gen)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/libs/go2idl/client-gen/args)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/libs/go2idl/client-gen/generators)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/libs/go2idl/client-gen/generators/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/libs/go2idl/client-gen/generators/scheme)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/libs/go2idl/client-gen/types)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/libs/go2idl/conversion-gen/generators)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/libs/go2idl/go-to-protobuf/protobuf)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/libs/go2idl/go-to-protobuf/protoc-gen-gogo)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/libs/go2idl/lister-gen/generators)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/cmd/libs/go2idl/openapi-gen/generators)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/apis/federation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/apis/federation/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/apis/federation/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/client/clientset_generated/federation_clientset)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/client/clientset_generated/federation_clientset/scheme)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/client/clientset_generated/federation_clientset/typed/autoscaling/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/client/clientset_generated/federation_clientset/typed/batch/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/client/clientset_generated/federation_clientset/typed/core/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/client/clientset_generated/federation_clientset/typed/extensions/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/client/clientset_generated/federation_clientset/typed/federation/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/client/clientset_generated/federation_internalclientset)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/client/clientset_generated/federation_internalclientset/scheme)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/client/clientset_generated/federation_internalclientset/typed/autoscaling/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/client/clientset_generated/federation_internalclientset/typed/batch/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/client/clientset_generated/federation_internalclientset/typed/core/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/client/clientset_generated/federation_internalclientset/typed/extensions/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/client/clientset_generated/federation_internalclientset/typed/federation/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/pkg/federation-controller/replicaset)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/pkg/federation-controller/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/pkg/federation-controller/util/deletionhelper)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/pkg/federation-controller/util/eventsink)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/pkg/federation-controller/util/planner)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/federation/pkg/federation-controller/util/podanalyzer)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/api)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/api/annotations)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/api/endpoints)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/api/events)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/api/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/api/pod)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/api/service)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/api/testapi)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/api/testing)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/api/testing/compat)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/api/unversioned)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/api/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/api/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/api/v1/endpoints)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/api/v1/pod)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/api/v1/service)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/api/v1/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/api/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/abac)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/abac/latest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/abac/v0)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/abac/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/apps)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/apps/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/apps/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/apps/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/authentication)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/authentication/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/authentication/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/authentication/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/authorization)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/authorization/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/authorization/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/authorization/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/authorization/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/autoscaling)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/autoscaling/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/autoscaling/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/autoscaling/v2alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/autoscaling/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/batch)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/batch/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/batch/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/batch/v2alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/batch/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/certificates)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/certificates/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/certificates/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/certificates/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/componentconfig)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/componentconfig/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/componentconfig/v1alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/extensions)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/extensions/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/extensions/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/extensions/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/imagepolicy)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/imagepolicy/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/imagepolicy/v1alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/policy)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/policy/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/policy/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/policy/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/rbac)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/rbac/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/rbac/v1alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/rbac/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/rbac/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/settings)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/settings/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/settings/v1alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/settings/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/storage/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/storage/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/storage/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/storage/v1/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/storage/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/apis/storage/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/auth/authorizer/abac)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/bootstrap/api)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/capabilities)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/chaosclient)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/scheme)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/apps/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/apps/v1beta1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/authentication/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/authentication/v1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/authentication/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/authentication/v1beta1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/authorization/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/authorization/v1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/authorization/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/authorization/v1beta1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/autoscaling/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/autoscaling/v1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/autoscaling/v2alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/autoscaling/v2alpha1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/batch/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/batch/v1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/batch/v2alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/batch/v2alpha1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/certificates/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/certificates/v1beta1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/core/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/core/v1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/extensions/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/extensions/v1beta1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/policy/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/policy/v1beta1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/rbac/v1alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/rbac/v1alpha1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/rbac/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/rbac/v1beta1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/settings/v1alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/settings/v1alpha1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/storage/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/storage/v1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/storage/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/clientset/typed/storage/v1beta1/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/scheme)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/apps/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/apps/internalversion/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/authentication/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/authentication/internalversion/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/authorization/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/authorization/internalversion/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/autoscaling/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/autoscaling/internalversion/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/batch/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/batch/internalversion/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/certificates/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/certificates/internalversion/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/core/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/core/internalversion/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/extensions/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/extensions/internalversion/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/policy/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/policy/internalversion/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/rbac/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/rbac/internalversion/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/settings/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/settings/internalversion/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/storage/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/typed/storage/internalversion/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/conditions)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/apps)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/apps/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/autoscaling)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/autoscaling/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/autoscaling/v2alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/batch)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/batch/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/batch/v2alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/certificates)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/certificates/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/core)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/core/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/extensions)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/extensions/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/internalinterfaces)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/policy)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/policy/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/rbac)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/rbac/v1alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/rbac/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/settings)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/settings/v1alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/storage/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/externalversions/storage/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/apps)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/apps/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/autoscaling)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/autoscaling/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/batch)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/batch/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/certificates)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/certificates/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/core)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/core/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/extensions)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/extensions/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/internalinterfaces)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/policy)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/policy/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/rbac)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/rbac/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/settings)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/settings/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/informers/informers_generated/internalversion/storage/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/leaderelection)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/leaderelection/resourcelock)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/apps/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/apps/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/autoscaling/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/autoscaling/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/autoscaling/v2alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/batch/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/batch/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/batch/v2alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/certificates/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/certificates/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/core/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/core/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/extensions/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/extensions/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/policy/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/policy/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/rbac/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/rbac/v1alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/rbac/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/settings/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/settings/v1alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/storage/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/storage/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/listers/storage/v1beta1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/retry)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/unversioned)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/client/unversioned/remotecommand)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/cloudprovider)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/cloudprovider/providers)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/cloudprovider/providers/aws)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/cloudprovider/providers/azure)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/cloudprovider/providers/cloudstack)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/cloudprovider/providers/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/cloudprovider/providers/gce)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/cloudprovider/providers/mesos)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/cloudprovider/providers/openstack)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/cloudprovider/providers/ovirt)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/cloudprovider/providers/photon)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/cloudprovider/providers/rackspace)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/cloudprovider/providers/vsphere)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/bootstrap)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/certificates)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/cronjob)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/daemon)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/daemon/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/deployment)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/deployment/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/disruption)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/endpoint)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/garbagecollector)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/garbagecollector/metaonly)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/job)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/namespace)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/namespace/deletion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/node)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/podautoscaler)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/podautoscaler/metrics)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/podgc)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/replicaset)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/replication)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/resourcequota)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/route)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/service)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/serviceaccount)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/statefulset)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/ttl)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/volume/attachdetach)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/volume/attachdetach/cache)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/volume/attachdetach/populator)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/volume/attachdetach/reconciler)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/volume/attachdetach/statusupdater)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/controller/volume/persistentvolume)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/credentialprovider)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/credentialprovider/aws)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/credentialprovider/azure)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/credentialprovider/gcp)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/features)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/fieldpath)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/generated)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/generated/openapi)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubeapiserver)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubeapiserver/admission)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubeapiserver/authenticator)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubeapiserver/authorizer)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubeapiserver/authorizer/modes)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubeapiserver/options)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubectl)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubectl/cmd)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubectl/cmd/auth)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubectl/cmd/config)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubectl/cmd/rollout)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubectl/cmd/set)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubectl/cmd/templates)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubectl/cmd/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubectl/cmd/util/editor)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubectl/metricsutil)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubectl/resource)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/api)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/api/v1alpha1/runtime)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/api/v1alpha1/stats)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/cadvisor)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/cadvisor/testing)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/client)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/cm)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/cm/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/config)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/container)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/container/testing)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/custommetrics)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/dockershim)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/dockershim/cm)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/dockershim/errors)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/dockershim/remote)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/dockertools)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/dockertools/securitycontext)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/envvars)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/events)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/eviction)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/eviction/api)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/gpu)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/gpu/nvidia)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/images)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/kuberuntime)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/leaky)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/lifecycle)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/metrics)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/network)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/network/cni)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/network/hairpin)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/network/hostport)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/network/kubenet)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/pleg)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/pod)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/preemption)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/prober)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/prober/results)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/qos)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/remote)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/rkt)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/secret)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/server)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/server/portforward)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/server/remotecommand)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/server/stats)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/server/streaming)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/status)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/sysctl)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/types)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/util/cache)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/util/csr)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/util/format)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/util/ioutils)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/util/queue)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/util/sliceutils)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/volumemanager)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/volumemanager/cache)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/volumemanager/populator)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/kubelet/volumemanager/reconciler)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/master)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/master/ports)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/master/thirdparty)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/master/tunneler)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/metrics)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/printers)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/printers/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/probe)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/probe/exec)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/probe/http)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/probe/tcp)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/proxy)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/proxy/config)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/proxy/healthcheck)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/proxy/iptables)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/proxy/userspace)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/proxy/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/proxy/winuserspace)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/quota)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/quota/evaluator/core)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/quota/generic)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/quota/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/apps/petset)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/apps/petset/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/apps/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/authentication/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/authentication/tokenreview)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/authorization/localsubjectaccessreview)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/authorization/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/authorization/selfsubjectaccessreview)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/authorization/subjectaccessreview)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/authorization/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/autoscaling/horizontalpodautoscaler)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/autoscaling/horizontalpodautoscaler/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/autoscaling/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/batch/cronjob)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/batch/cronjob/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/batch/job)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/batch/job/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/batch/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/cachesize)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/certificates/certificates)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/certificates/certificates/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/certificates/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/componentstatus)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/configmap)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/configmap/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/endpoint)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/endpoint/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/event)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/event/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/limitrange)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/limitrange/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/namespace)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/namespace/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/node)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/node/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/node/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/persistentvolume)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/persistentvolume/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/persistentvolumeclaim)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/persistentvolumeclaim/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/pod)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/pod/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/pod/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/podtemplate)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/podtemplate/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/rangeallocation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/replicationcontroller)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/replicationcontroller/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/resourcequota)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/resourcequota/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/secret)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/secret/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/service)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/service/allocator)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/service/allocator/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/service/ipallocator)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/service/ipallocator/controller)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/service/portallocator)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/service/portallocator/controller)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/service/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/serviceaccount)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/core/serviceaccount/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/extensions/controller/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/extensions/daemonset)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/extensions/daemonset/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/extensions/deployment)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/extensions/deployment/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/extensions/ingress)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/extensions/ingress/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/extensions/networkpolicy)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/extensions/networkpolicy/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/extensions/podsecuritypolicy)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/extensions/podsecuritypolicy/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/extensions/replicaset)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/extensions/replicaset/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/extensions/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/extensions/thirdpartyresource)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/extensions/thirdpartyresource/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/extensions/thirdpartyresourcedata)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/extensions/thirdpartyresourcedata/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/policy/poddisruptionbudget)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/policy/poddisruptionbudget/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/policy/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/rbac)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/rbac/clusterrole)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/rbac/clusterrole/policybased)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/rbac/clusterrole/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/rbac/clusterrolebinding)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/rbac/clusterrolebinding/policybased)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/rbac/clusterrolebinding/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/rbac/reconciliation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/rbac/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/rbac/role)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/rbac/role/policybased)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/rbac/role/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/rbac/rolebinding)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/rbac/rolebinding/policybased)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/rbac/rolebinding/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/rbac/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/registrytest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/securitycontextconstraints)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/securitycontextconstraints/etcd)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/settings/podpreset)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/settings/podpreset/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/settings/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/storage/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/storage/storageclass)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/registry/storage/storageclass/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/routes)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/runtime/serializer/json)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/runtime/serializer/protobuf)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/security/apparmor)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/security/podsecuritypolicy)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/security/podsecuritypolicy/apparmor)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/security/podsecuritypolicy/capabilities)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/security/podsecuritypolicy/group)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/security/podsecuritypolicy/seccomp)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/security/podsecuritypolicy/selinux)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/security/podsecuritypolicy/sysctl)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/security/podsecuritypolicy/user)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/security/podsecuritypolicy/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/securitycontext)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/securitycontextconstraints)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/securitycontextconstraints/capabilities)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/securitycontextconstraints/group)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/securitycontextconstraints/seccomp)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/securitycontextconstraints/selinux)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/securitycontextconstraints/user)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/securitycontextconstraints/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/serviceaccount)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/ssh)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/async)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/bandwidth)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/chmod)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/chown)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/config)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/configz)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/crlf)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/dbus)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/ebtables)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/env)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/exec)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/flock)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/goroutinemap)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/goroutinemap/exponentialbackoff)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/hash)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/i18n)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/interrupt)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/io)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/ipconfig)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/iptables)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/keymutex)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/labels)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/limitwriter)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/logs)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/maps)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/metrics)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/mount)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/net/sets)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/netsh)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/node)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/oom)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/parsers)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/procfs)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/resourcecontainer)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/rlimit)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/selinux)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/slice)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/strings)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/sysctl)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/system)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/tail)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/taints)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/term)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/version)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/util/workqueue/prometheus)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/version)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/aws_ebs)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/azure_dd)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/azure_file)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/cephfs)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/cinder)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/configmap)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/downwardapi)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/empty_dir)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/fc)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/flexvolume)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/flocker)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/gce_pd)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/git_repo)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/glusterfs)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/host_path)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/iscsi)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/nfs)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/photon_pd)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/portworx)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/projected)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/quobyte)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/rbd)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/scaleio)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/secret)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/util/nestedpendingoperations)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/util/operationexecutor)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/util/types)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/util/volumehelper)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/volume/vsphere_volume)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/pkg/watch/json)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/cmd/kube-scheduler/app)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/cmd/kube-scheduler/app/options)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/admit)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/alwayspullimages)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/antiaffinity)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/defaulttolerationseconds)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/deny)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/exec)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/gc)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/imagepolicy)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/initialresources)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/limitranger)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/namespace/autoprovision)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/namespace/exists)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/namespace/lifecycle)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/persistentvolume/label)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/podnodeselector)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/podpreset)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/resourcequota)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/resourcequota/apis/resourcequota)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/resourcequota/apis/resourcequota/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/resourcequota/apis/resourcequota/v1alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/resourcequota/apis/resourcequota/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/security/podsecuritypolicy)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/securitycontext/scdeny)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/serviceaccount)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/admission/storageclass/default)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/auth/authenticator/token/bootstrap)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/auth/authorizer/rbac)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/auth/authorizer/rbac/bootstrappolicy)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/scheduler)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/scheduler/algorithm)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/scheduler/algorithm/predicates)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/scheduler/algorithm/priorities)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/scheduler/algorithm/priorities/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/scheduler/algorithmprovider)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/scheduler/algorithmprovider/defaults)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/scheduler/api)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/scheduler/api/latest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/scheduler/api/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/scheduler/api/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/scheduler/core)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/scheduler/factory)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/scheduler/metrics)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/scheduler/schedulercache)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/plugin/pkg/scheduler/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/api/errors)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/api/meta)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/api/resource)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/api/testing)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/api/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/api/validation/path)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/apimachinery)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/apimachinery/announced)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/apimachinery/registered)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/apis/meta/internalversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/apis/meta/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/apis/meta/v1/unstructured)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/apis/meta/v1/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/conversion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/conversion/queryparams)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/fields)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/labels)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/openapi)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/runtime)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/runtime/schema)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/runtime/serializer)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/selection)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/types)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/diff)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/errors)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/httpstream/spdy)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/intstr)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/net)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/rand)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/runtime)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/sets)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/strategicpatch)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/uuid)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/validation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/validation/field)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/wait)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/util/yaml)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/version)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/pkg/watch)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apimachinery/third_party/forked/golang/netutil)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/admission)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/apis/apiserver)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/apis/apiserver/install)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/apis/apiserver/v1alpha1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authentication/authenticator)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authentication/group)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authentication/request/headerrequest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authentication/request/union)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authentication/serviceaccount)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authentication/user)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/authorization/authorizer)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/endpoints)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/endpoints/filters)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/endpoints/handlers)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/endpoints/openapi)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/endpoints/request)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/registry/generic)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/registry/generic/registry)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/registry/generic/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/registry/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/server)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/server/filters)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/server/healthz)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/server/httplog)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/server/mux)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/server/options)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/server/routes)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/server/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage/etcd)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage/etcd/etcdtest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage/etcd/testing)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage/etcd/util)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage/names)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage/storagebackend)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/storage/storagebackend/factory)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/util/feature)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/apiserver/pkg/util/flag)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/discovery)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/discovery/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/dynamic)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/kubernetes/typed/core/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/api)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/pkg/api/v1)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/plugin/pkg/client/auth)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/rest)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/rest/fake)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/testing)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/tools/cache)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/tools/clientcmd)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/tools/clientcmd/api)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/tools/portforward)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/tools/record)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/transport)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/util/cert)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/util/clock)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/util/flowcontrol)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/util/homedir)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/util/integer)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/util/jsonpath)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/util/testing)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/staging/src/k8s.io/client-go/util/workqueue)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/test/e2e)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/test/e2e/chaosmonkey)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/test/e2e/common)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/test/e2e/framework)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/test/e2e/generated)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/test/e2e/perftype)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/test/e2e/scheduling)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/test/e2e/upgrades)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/test/e2e_federation)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/test/e2e_federation/framework)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/test/e2e_federation/upgrades)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/test/images/net/common)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/test/images/net/nat)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/test/utils)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/kubernetes/third_party/forked/golang/expansion)) = fff65cf41bdeeaff9964af98450b254f3f2da553
Provides: bundled(golang(k8s.io/metrics/pkg/apis/custom_metrics)) = fd2415bb9381a6731027b48a8c6b78f28e13f876
Provides: bundled(golang(k8s.io/metrics/pkg/apis/custom_metrics/install)) = fd2415bb9381a6731027b48a8c6b78f28e13f876
Provides: bundled(golang(k8s.io/metrics/pkg/apis/custom_metrics/v1alpha1)) = fd2415bb9381a6731027b48a8c6b78f28e13f876
Provides: bundled(golang(k8s.io/metrics/pkg/apis/metrics)) = fd2415bb9381a6731027b48a8c6b78f28e13f876
Provides: bundled(golang(k8s.io/metrics/pkg/apis/metrics/install)) = fd2415bb9381a6731027b48a8c6b78f28e13f876
Provides: bundled(golang(k8s.io/metrics/pkg/apis/metrics/v1alpha1)) = fd2415bb9381a6731027b48a8c6b78f28e13f876
Provides: bundled(golang(k8s.io/metrics/pkg/client/clientset_generated/clientset)) = fd2415bb9381a6731027b48a8c6b78f28e13f876
Provides: bundled(golang(k8s.io/metrics/pkg/client/clientset_generated/clientset/fake)) = fd2415bb9381a6731027b48a8c6b78f28e13f876
Provides: bundled(golang(k8s.io/metrics/pkg/client/clientset_generated/clientset/scheme)) = fd2415bb9381a6731027b48a8c6b78f28e13f876
Provides: bundled(golang(k8s.io/metrics/pkg/client/clientset_generated/clientset/typed/metrics/v1alpha1)) = fd2415bb9381a6731027b48a8c6b78f28e13f876
Provides: bundled(golang(k8s.io/metrics/pkg/client/clientset_generated/clientset/typed/metrics/v1alpha1/fake)) = fd2415bb9381a6731027b48a8c6b78f28e13f876
Provides: bundled(golang(k8s.io/metrics/pkg/client/custom_metrics)) = fd2415bb9381a6731027b48a8c6b78f28e13f876
Provides: bundled(golang(k8s.io/metrics/pkg/client/custom_metrics/fake)) = fd2415bb9381a6731027b48a8c6b78f28e13f876
Provides: bundled(golang(vbom.ml/util/sortorder)) = db5cfe13f5cc80a4990d98e2e1b0707a4d1a5394
Provides: bundled(golang(golang.org/x/time/rate)) = f51c12702a4d776e4c1fa9b0fabab841babae631

%description
OpenShift Origin is a distribution of Kubernetes optimized for application
development and deployment. OpenShift Origin adds developer and operational
centric tools on top of Kubernetes to enable rapid application development, easy
deployment and scaling, and long-term lifecycle maintenance for small and large
teams and applications. It provides a secure and multi-tenant configuration for
Kubernetes allowing you to safely host many different applications and workloads
on a unified cluster.

%package master
Summary:        %{product_name} Master
Requires:       %{name} = %{version}-%{release}
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd
Obsoletes:      openshift-master < %{package_refector_version}

%description master
%{summary}

%if 0%{build_tests}
%package tests
Summary: %{product_name} Test Suite
Requires:       %{name} = %{version}-%{release}

%description tests
%{summary}
%endif

%package node
Summary:        %{product_name} Node
Requires:       %{name} = %{version}-%{release}
Requires:       docker >= %{docker_version}
Requires:       util-linux
Requires:       socat
Requires:       nfs-utils
Requires:       ethtool
%if 0%{?suse_version}
# device-mapper-persistent-tools is bundled into device-mapper
Requires:       device-mapper
%else
Requires:       device-mapper-persistent-data >= 0.6.2
%endif
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd
Obsoletes:      openshift-node < %{package_refector_version}
Obsoletes:      tuned-profiles-%{name}-node < 3.6
Provides:       tuned-profiles-%{name}-node %{version}

%description node
%{summary}

%package clients
Summary:        %{product_name} Client binaries for Linux
Obsoletes:      openshift-clients < %{package_refector_version}
Requires:       git
Requires:       bash-completion

%description clients
%{summary}

%if 0%{?make_redistributable}
%package clients-redistributable
Summary:        %{product_name} Client binaries for Linux, Mac OSX, and Windows
Obsoletes:      openshift-clients-redistributable < %{package_refector_version}

%description clients-redistributable
%{summary}
%endif

%package dockerregistry
Summary:        Docker Registry v2 for %{product_name}
Requires:       %{name} = %{version}-%{release}

%description dockerregistry
%{summary}

%package pod
Summary:        %{product_name} Pod

%description pod
%{summary}

%package sdn-ovs
Summary:          %{product_name} SDN Plugin for Open vSwitch
Requires:         openvswitch >= %{openvswitch_version}
Requires:         %{name}-node = %{version}-%{release}
Requires:         bridge-utils
Requires:         bind-utils
Requires:         ethtool
%if 0%{?suse_version}
Requires:         procps
%else
Requires:         procps-ng
%endif
%if 0%{?mageia} || 0%{?suse_version}
Requires:         iproute2
%else
Requires:         iproute
%endif
Obsoletes:        openshift-sdn-ovs < %{package_refector_version}

%description sdn-ovs
%{summary}

%package federation-services
Summary:        %{produce_name} Federation Services
Requires:       %{name} = %{version}-%{release}

%description federation-services

%package service-catalog
Summary:        %{product_name} Service Catalog
Requires:       %{name} = %{version}-%{release}

%description service-catalog
%{summary}

%package cluster-capacity
Summary:        %{product_name} Cluster Capacity Analysis Tool
Requires:       %{name} = %{version}-%{release}

%description cluster-capacity
%{summary}

%if 0%{?fedora} || 0%{?rhel} || 0%{?mageia} >= 6
%package excluder
Summary:   Exclude openshift packages from updates
BuildArch: noarch

%description excluder
Many times admins do not want openshift updated when doing
normal system updates.

%{name}-excluder exclude - No openshift packages can be updated
%{name}-excluder unexclude - Openshift packages can be updated

%package docker-excluder
Summary:   Exclude docker packages from updates
BuildArch: noarch

%description docker-excluder
Certain versions of OpenShift will not work with newer versions
of docker.  Exclude those versions of docker.

%{name}-docker-excluder exclude - No major docker updates
%{name}-docker-excluder unexclude - docker packages can be updated
%endif

%package web-console
Summary: Web Console for the OpenShift Application Platform

%description web-console
OpenShift is a distribution of Kubernetes optimized for enterprise application
development and deployment. This is the web console server for OpenShift.


%prep
%setup -q -n %{name}-%{commit}
gzip -dc %{SOURCE1} | tar -xof -
gzip -dc %{SOURCE2} | tar -xof -

%patch0 -p1 -b .bsfix
%patch1 -p1 -b .32b

%if 0%{?suse_version}
mkdir -p $HOME/go/src/github.com/openshift
ln -sr $PWD $HOME/go/src/%{import_path}
%endif

%build
echo "GOLANG DEBUG OUTPUT"
go version
export TRAVIS=true
%if 0%{make_redistributable}
# Create Binaries for all supported arches
%{os_git_vars} OS_BUILD_RELEASE_ARCHIVES=n make build-cross
%{os_git_vars} hack/build-go.sh vendor/github.com/onsi/ginkgo/ginkgo
pushd image-registry-%{registry_commit}
%{os_git_vars} OS_BUILD_RELEASE_ARCHIVES=n make build-cross
popd
pushd origin-web-console-server-%{webconsole_commit}
%{os_git_vars} OS_BUILD_RELEASE_ARCHIVES=n make build-cross
popd
%{os_git_vars} unset GOPATH; cmd/service-catalog/go/src/github.com/kubernetes-incubator/service-catalog/hack/build-cross.sh
%{os_git_vars} unset GOPATH; cmd/cluster-capacity/go/src/github.com/kubernetes-incubator/cluster-capacity/hack/build-cross.sh
%else
# Create Binaries only for building arch
%ifarch x86_64
  BUILD_PLATFORM="linux/amd64"
%endif
%ifarch %{ix86}
  BUILD_PLATFORM="linux/386"
%endif
%ifarch ppc64le
  BUILD_PLATFORM="linux/ppc64le"
%endif
%ifarch aarch64
  BUILD_PLATFORM="linux/arm64"
%endif
%ifarch %{arm}
  BUILD_PLATFORM="linux/arm"
%endif
%ifarch s390x
  BUILD_PLATFORM="linux/s390x"
%endif
OS_ONLY_BUILD_PLATFORMS="${BUILD_PLATFORM}" %{os_git_vars} OS_BUILD_RELEASE_ARCHIVES=n make build-cross
pushd image-registry-%{registry_commit}
OS_ONLY_BUILD_PLATFORMS="${BUILD_PLATFORM}" %{os_git_vars} OS_BUILD_RELEASE_ARCHIVES=n make build-cross
popd
pushd origin-web-console-server-%{webconsole_commit}
OS_ONLY_BUILD_PLATFORMS="${BUILD_PLATFORM}" %{os_git_vars} OS_BUILD_RELEASE_ARCHIVES=n make build-cross
popd
OS_ONLY_BUILD_PLATFORMS="${BUILD_PLATFORM}" %{os_git_vars} hack/build-go.sh vendor/github.com/onsi/ginkgo/ginkgo
OS_ONLY_BUILD_PLATFORMS="${BUILD_PLATFORM}" %{os_git_vars} unset GOPATH; cmd/service-catalog/go/src/github.com/kubernetes-incubator/service-catalog/hack/build-cross.sh
OS_ONLY_BUILD_PLATFORMS="${BUILD_PLATFORM}" %{os_git_vars} unset GOPATH; cmd/cluster-capacity/go/src/github.com/kubernetes-incubator/cluster-capacity/hack/build-cross.sh
%endif


%if 0%{build_tests}
# Create extended.test
%{os_git_vars} hack/build-go.sh test/extended/extended.test
%endif

# Create/Update man pages
%{os_git_vars} hack/generate-docs.sh

%install

PLATFORM="$(go env GOHOSTOS)/$(go env GOHOSTARCH)"
install -d %{buildroot}%{_bindir}

# Install linux components
for bin in oc openshift
do
  echo "+++ INSTALLING ${bin}"
  install -p -m 755 _output/local/bin/${PLATFORM}/${bin} %{buildroot}%{_bindir}/${bin}
done

echo "+++ INSTALLING dockerregistry"
install -p -m 755 image-registry-%{registry_commit}/_output/local/bin/${PLATFORM}/dockerregistry %{buildroot}%{_bindir}/dockerregistry

echo "+++ INSTALLING web-console"
install -p -m 755 origin-web-console-server-%{webconsole_commit}/_output/local/bin/${PLATFORM}/origin-web-console %{buildroot}%{_bindir}/origin-web-console

install -d %{buildroot}%{_libexecdir}/%{name}
%if 0%{build_tests}
install -p -m 755 _output/local/bin/${PLATFORM}/extended.test %{buildroot}%{_libexecdir}/%{name}/
%endif

%if 0%{?make_redistributable}
# Install client executable for windows and mac
install -d %{buildroot}%{_datadir}/%{name}/{linux,macosx,windows}
install -p -m 755 _output/local/bin/linux/*/oc %{buildroot}%{_datadir}/%{name}/linux/oc
install -p -m 755 _output/local/bin/darwin/*/oc %{buildroot}/%{_datadir}/%{name}/macosx/oc
install -p -m 755 _output/local/bin/windows/*/oc.exe %{buildroot}/%{_datadir}/%{name}/windows/oc.exe
%endif

# Install federation services
install -p -m 755 _output/local/bin/${PLATFORM}/hyperkube %{buildroot}%{_bindir}/

# Install cluster capacity
install -p -m 755 cmd/cluster-capacity/go/src/github.com/kubernetes-incubator/cluster-capacity/_output/local/bin/${PLATFORM}/hypercc %{buildroot}%{_bindir}/
ln -s hypercc %{buildroot}%{_bindir}/cluster-capacity

# Install service-catalog
install -p -m 755 cmd/service-catalog/go/src/github.com/kubernetes-incubator/service-catalog/_output/local/bin/${PLATFORM}/service-catalog %{buildroot}%{_bindir}/

# Install pod
install -p -m 755 _output/local/bin/${PLATFORM}/pod %{buildroot}%{_bindir}/

install -d -m 0755 %{buildroot}%{_unitdir}

mkdir -p %{buildroot}%{_sysconfdir}/sysconfig

for cmd in \
    kube-controller-manager \
    kube-proxy \
    kube-scheduler \
    kubelet \
    kubernetes \
    oadm \
    openshift-deploy \
    openshift-docker-build \
    openshift-f5-router \
    openshift-recycle \
    openshift-router \
    openshift-sti-build \
    origin
do
    ln -s openshift %{buildroot}%{_bindir}/$cmd
done

ln -s oc %{buildroot}%{_bindir}/kubectl

install -d -m 0755 %{buildroot}%{_sysconfdir}/origin/{master,node}

# different service for origin vs aos
install -m 0644 contrib/systemd/%{name}-master.service %{buildroot}%{_unitdir}/%{name}-master.service
install -m 0644 contrib/systemd/%{name}-node.service %{buildroot}%{_unitdir}/%{name}-node.service
# same sysconfig files for origin vs aos
install -m 0644 contrib/systemd/origin-master.sysconfig %{buildroot}%{_sysconfdir}/sysconfig/%{name}-master
install -m 0644 contrib/systemd/origin-node.sysconfig %{buildroot}%{_sysconfdir}/sysconfig/%{name}-node

# Install man1 man pages
install -d -m 0755 %{buildroot}%{_mandir}/man1
install -m 0644 docs/man/man1/* %{buildroot}%{_mandir}/man1/

mkdir -p %{buildroot}%{_sharedstatedir}/origin

# Install sdn scripts
install -d -m 0755 %{buildroot}%{_sysconfdir}/cni/net.d
install -d -m 0755 %{buildroot}/opt/cni/bin
install -p -m 0755 _output/local/bin/linux/*/sdn-cni-plugin %{buildroot}/opt/cni/bin/openshift-sdn
install -p -m 0755 _output/local/bin/linux/*/host-local %{buildroot}/opt/cni/bin
install -p -m 0755 _output/local/bin/linux/*/loopback %{buildroot}/opt/cni/bin

install -d -m 0755 %{buildroot}%{_unitdir}/%{name}-node.service.d
install -p -m 0644 contrib/systemd/openshift-sdn-ovs.conf %{buildroot}%{_unitdir}/%{name}-node.service.d/openshift-sdn-ovs.conf

# Install bash completions
install -d -m 755 %{buildroot}%{_sysconfdir}/bash_completion.d/
for bin in oadm oc openshift
do
  echo "+++ INSTALLING BASH COMPLETIONS FOR ${bin} "
  %{buildroot}%{_bindir}/${bin} completion bash > %{buildroot}%{_sysconfdir}/bash_completion.d/${bin}
  chmod 644 %{buildroot}%{_sysconfdir}/bash_completion.d/${bin}
done

# Install origin-accounting
install -d -m 755 %{buildroot}%{_sysconfdir}/systemd/system.conf.d/
install -p -m 644 contrib/systemd/origin-accounting.conf %{buildroot}%{_sysconfdir}/systemd/system.conf.d/

%if 0%{?fedora} || 0%{?rhel} || 0%{?mageia} >= 6
# Excluder variables
mkdir -p $RPM_BUILD_ROOT/usr/sbin
%if 0%{?fedora} || 0%{?mageia} >= 6
  OS_CONF_FILE="/etc/dnf/dnf.conf"
%else
  OS_CONF_FILE="/etc/yum.conf"
%endif

# Install openshift-excluder script
sed "s|@@CONF_FILE-VARIABLE@@|${OS_CONF_FILE}|" contrib/excluder/excluder-template > $RPM_BUILD_ROOT/usr/sbin/%{name}-excluder

chmod 0744 $RPM_BUILD_ROOT/usr/sbin/%{name}-excluder

# Install docker-excluder script
sed "s|@@CONF_FILE-VARIABLE@@|${OS_CONF_FILE}|" contrib/excluder/excluder-template > $RPM_BUILD_ROOT/usr/sbin/%{name}-docker-excluder
sed -i "s|@@PACKAGE_LIST-VARIABLE@@|docker*1.13* docker*1.14* docker*1.15* docker*1.16* docker*1.17* docker*1.18* docker*1.19* docker*1.20*|" $RPM_BUILD_ROOT/usr/sbin/%{name}-docker-excluder
chmod 0744 $RPM_BUILD_ROOT/usr/sbin/%{name}-docker-excluder
%endif

# Install migration scripts
install -d %{buildroot}%{_datadir}/%{name}/migration
install -p -m 755 contrib/migration/* %{buildroot}%{_datadir}/%{name}/migration/

%files
%doc README.md
%license LICENSE
%{_bindir}/openshift
%{_bindir}/kube-controller-manager
%{_bindir}/kube-proxy
%{_bindir}/kube-scheduler
%{_bindir}/kubelet
%{_bindir}/kubernetes
%{_bindir}/oadm
%{_bindir}/openshift-deploy
%{_bindir}/openshift-docker-build
%{_bindir}/openshift-f5-router
%{_bindir}/openshift-recycle
%{_bindir}/openshift-router
%{_bindir}/openshift-sti-build
%{_bindir}/origin
%{_sharedstatedir}/origin
%{_sysconfdir}/bash_completion.d/oadm
%{_sysconfdir}/bash_completion.d/openshift
%defattr(-,root,root,0700)
%dir %config(noreplace) %{_sysconfdir}/origin
%ghost %dir %config(noreplace) %{_sysconfdir}/origin
%ghost %config(noreplace) %{_sysconfdir}/origin/.config_managed
%{_mandir}/man1/openshift*

%pre
# If /etc/openshift exists and /etc/origin doesn't, symlink it to /etc/origin
if [ -d "%{_sysconfdir}/openshift" ]; then
  if ! [ -d "%{_sysconfdir}/origin"  ]; then
    ln -s %{_sysconfdir}/openshift %{_sysconfdir}/origin
  fi
fi
if [ -d "%{_sharedstatedir}/openshift" ]; then
  if ! [ -d "%{_sharedstatedir}/origin"  ]; then
    ln -s %{_sharedstatedir}/openshift %{_sharedstatedir}/origin
  fi
fi

%if 0%{build_tests}
%files tests
%license LICENSE
%{_libexecdir}/%{name}
%{_libexecdir}/%{name}/extended.test
%endif

%files master
%license LICENSE
%{_unitdir}/%{name}-master.service
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}-master
%dir %{_datadir}/%{name}/migration/
%{_datadir}/%{name}/migration/*
%defattr(-,root,root,0700)
%config(noreplace) %{_sysconfdir}/origin/master
%ghost %config(noreplace) %{_sysconfdir}/origin/admin.crt
%ghost %config(noreplace) %{_sysconfdir}/origin/admin.key
%ghost %config(noreplace) %{_sysconfdir}/origin/admin.kubeconfig
%ghost %config(noreplace) %{_sysconfdir}/origin/ca.crt
%ghost %config(noreplace) %{_sysconfdir}/origin/ca.key
%ghost %config(noreplace) %{_sysconfdir}/origin/ca.serial.txt
%ghost %config(noreplace) %{_sysconfdir}/origin/etcd.server.crt
%ghost %config(noreplace) %{_sysconfdir}/origin/etcd.server.key
%ghost %config(noreplace) %{_sysconfdir}/origin/master-config.yaml
%ghost %config(noreplace) %{_sysconfdir}/origin/master.etcd-client.crt
%ghost %config(noreplace) %{_sysconfdir}/origin/master.etcd-client.key
%ghost %config(noreplace) %{_sysconfdir}/origin/master.kubelet-client.crt
%ghost %config(noreplace) %{_sysconfdir}/origin/master.kubelet-client.key
%ghost %config(noreplace) %{_sysconfdir}/origin/master.server.crt
%ghost %config(noreplace) %{_sysconfdir}/origin/master.server.key
%ghost %config(noreplace) %{_sysconfdir}/origin/openshift-master.crt
%ghost %config(noreplace) %{_sysconfdir}/origin/openshift-master.key
%ghost %config(noreplace) %{_sysconfdir}/origin/openshift-master.kubeconfig
%ghost %config(noreplace) %{_sysconfdir}/origin/openshift-registry.crt
%ghost %config(noreplace) %{_sysconfdir}/origin/openshift-registry.key
%ghost %config(noreplace) %{_sysconfdir}/origin/openshift-registry.kubeconfig
%ghost %config(noreplace) %{_sysconfdir}/origin/openshift-router.crt
%ghost %config(noreplace) %{_sysconfdir}/origin/openshift-router.key
%ghost %config(noreplace) %{_sysconfdir}/origin/openshift-router.kubeconfig
%ghost %config(noreplace) %{_sysconfdir}/origin/policy.json
%ghost %config(noreplace) %{_sysconfdir}/origin/serviceaccounts.private.key
%ghost %config(noreplace) %{_sysconfdir}/origin/serviceaccounts.public.key
%ghost %config(noreplace) %{_sysconfdir}/origin/.config_managed

%post master
%systemd_post %{name}-master.service
# Create master config and certs if both do not exist
if [[ ! -e %{_sysconfdir}/origin/master/master-config.yaml &&
     ! -e %{_sysconfdir}/origin/master/ca.crt ]]; then
  %{_bindir}/openshift start master --write-config=%{_sysconfdir}/origin/master
  # Create node configs if they do not already exist
  if ! find %{_sysconfdir}/origin/ -type f -name "node-config.yaml" | grep -E "node-config.yaml"; then
    %{_bindir}/oadm create-node-config --node-dir=%{_sysconfdir}/origin/node/ --node=localhost --hostnames=localhost,127.0.0.1 --node-client-certificate-authority=%{_sysconfdir}/origin/master/ca.crt --signer-cert=%{_sysconfdir}/origin/master/ca.crt --signer-key=%{_sysconfdir}/origin/master/ca.key --signer-serial=%{_sysconfdir}/origin/master/ca.serial.txt --certificate-authority=%{_sysconfdir}/origin/master/ca.crt
  fi
  # Generate a marker file that indicates config and certs were RPM generated
  echo "# Config generated by RPM at "`date -u` > %{_sysconfdir}/origin/.config_managed
fi


%preun master
%systemd_preun %{name}-master.service

%postun master
%systemd_postun

%files node
%license LICENSE
%{_unitdir}/%{name}-node.service
%{_sysconfdir}/systemd/system.conf.d/origin-accounting.conf
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}-node
%defattr(-,root,root,0700)
%config(noreplace) %{_sysconfdir}/origin/node
%ghost %config(noreplace) %{_sysconfdir}/origin/node/node-config.yaml
%ghost %config(noreplace) %{_sysconfdir}/origin/.config_managed

%post node
%systemd_post %{name}-node.service
# If accounting is not currently enabled systemd reexec
if [[ `systemctl show docker %{name}-node | grep -q -e CPUAccounting=no -e MemoryAccounting=no; echo $?` == 0 ]]; then
  systemctl daemon-reexec
fi

%preun node
%systemd_preun %{name}-node.service

%postun node
%systemd_postun

%files sdn-ovs
%license LICENSE
%dir %{_unitdir}/%{name}-node.service.d/
%dir /opt/cni/bin
%dir %{_sysconfdir}/cni/net.d
%{_unitdir}/%{name}-node.service.d/openshift-sdn-ovs.conf
/opt/cni/bin/*

%posttrans sdn-ovs
# This path was installed by older packages but the directory wasn't owned by
# RPM so we need to clean it up otherwise kubelet throws an error trying to
# load the directory as a plugin
if [ -d %{kube_plugin_path} ]; then
  rmdir %{kube_plugin_path}
fi

%files service-catalog
%license LICENSE
%{_bindir}/service-catalog

%files clients
%license LICENSE
%{_bindir}/oc
%{_bindir}/kubectl
%{_sysconfdir}/bash_completion.d/oc
%{_mandir}/man1/oc*

%if 0%{?make_redistributable}
%files clients-redistributable
%license LICENSE
%dir %{_datadir}/%{name}/linux/
%dir %{_datadir}/%{name}/macosx/
%dir %{_datadir}/%{name}/windows/
%{_datadir}/%{name}/linux/oc
%{_datadir}/%{name}/macosx/oc
%{_datadir}/%{name}/windows/oc.exe
%endif

%files dockerregistry
%license LICENSE
%{_bindir}/dockerregistry

%files pod
%license LICENSE
%{_bindir}/pod

%if 0%{?fedora} || 0%{?rhel} || 0%{?mageia} >= 6
%files excluder
%license LICENSE
/usr/sbin/%{name}-excluder

%pretrans excluder
# we always want to clear this out using the last
#   versions script.  Otherwise excludes might get left in
if [ -s /usr/sbin/%{name}-excluder ] ; then
    /usr/sbin/%{name}-excluder unexclude
fi

%posttrans excluder
# we always want to run this after an install or update
/usr/sbin/%{name}-excluder exclude

%preun excluder
# If we are the last one, clean things up
if [ "$1" -eq 0 ] ; then
  /usr/sbin/%{name}-excluder unexclude
fi

%files docker-excluder
%license LICENSE
/usr/sbin/%{name}-docker-excluder

%pretrans docker-excluder
# we always want to clear this out using the last
#   versions script.  Otherwise excludes might get left in
if [ -s /usr/sbin/%{name}-docker-excluder ] ; then
    /usr/sbin/%{name}-docker-excluder unexclude
fi

%posttrans docker-excluder
# we always want to run this after an install or update
/usr/sbin/%{name}-docker-excluder exclude

%preun docker-excluder
# If we are the last one, clean things up
if [ "$1" -eq 0 ] ; then
    /usr/sbin/%{name}-docker-excluder unexclude
fi
%endif

%files cluster-capacity
%license LICENSE
%{_bindir}/hypercc
%{_bindir}/cluster-capacity

%files web-console
%license LICENSE
%{_bindir}/origin-web-console

%files federation-services
%{_bindir}/hyperkube

%changelog
* Sun Jun 10 2018 Neal Gompa <ngompa13@gmail.com> - 3.9.0-0.0.1
- Build in COPR for Mageia and openSUSE Leap

* Wed May 30 2018 Jakub Čajka <jcajka@fedoraproject.org> - 3.9.0-2
- Add web-console sub-package

* Fri Mar 23 2018 Jakub Čajka <jcajka@fedoraproject.org> - 3.9.0-1
- Rebase to 3.9.0
- Obsolete tuned-profiles-origin-node

* Fri Feb 09 2018 Igor Gnatenko <ignatenkobrain@fedoraproject.org> - 3.6.0-3
- Escape macros in %%changelog

* Thu Feb 08 2018 Fedora Release Engineering <releng@fedoraproject.org> - 3.6.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Tue Aug 08 2017 Adam Miller <maxamillion@fedoraproject.org> - 3.6.0-1
- Update to latest upstream
- Switch to new upstream versioning scheme (jump from 1.5 -> 3.6)

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.5.1-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.5.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Fri Jul 21 2017 Adam Miller <maxamillion@fedoraproject.org> - 1.5.1-3
- fix OS_CONF_FILE excluder path

* Wed Jul 05 2017 Adam Miller <maxamillion@fedoraproject.org> - 1.5.1-2
- Exclude ppc64 since docker doesn't exist for that architecture

* Mon Jun 26 2017 Adam Miller <maxamillion@fedoraproject.org> - 1.5.1-1
- Update to latest upstream - 1.5.1

* Tue Apr 25 2017 Adam Miller <maxamillion@fedoraproject.org> - 1.5.0-1
- Update to latest upstream - 1.5.0

* Thu Feb 16 2017 Adam Miller <maxamillion@fedoraproject.org> - 1.4.1-1
- Update to latest upstream - 1.4.1

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.4.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Jan 23 2017 Adam Miller <maxamillion@fedoraproject.org> - 1.4.0-1
- Update to latest upstream - 1.4.0

* Tue Oct 25 2016 Adam Miller <maxamillion@fedoraproject.org> - 1.3.1-1
- Update to latest upstream - 1.3.1

* Fri Sep 16 2016 Adam Miller <maxamillion@fedoraproject.org> - 1.3.0-1
- Update to latest upstream - 1.3.0
- Rebase spec file on upstream spec

* Thu Jul 21 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.0-2.git.0.2e62fab
- https://fedoraproject.org/wiki/Changes/golang1.7

* Fri Jun 17 2016 Adam Miller <maxamillion@fedoraproject.org- 1.2.0-1.git.0.2e62fab
- build on i686, %%{arm}, aarch64

* Thu Apr 21 2016 Dennis Gilmore <dennis@ausil.us> - 1.1.6-2.git.0.ef1caba
- build on i686, %%{arm}, aarch64

* Tue Apr 19 2016 Adam Miller <maxamillion@fedoraproject.org> - 1.1.6-1.git.0.ef1caba
- Update to latest upstream release

* Wed Mar 23 2016 Adam Miller <maxamillion@fedoraproject.org> - 1.1.4-1.git.0.3941102
- Update to latest upstream release

* Mon Feb 22 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1.3-2.git.0.cffae05
- https://fedoraproject.org/wiki/Changes/golang1.6

* Wed Feb 17 2016 Adam Miller <maxamillion@fedoraproject.org> - 1.1.3-1.git.0.cffae05
- Update to latest upstream release

* Tue Feb 09 2016 Adam Miller <maxamillion@fedoraproject.org> - 1.1.1-1.git.0.86b5e46
- Update to latest upstream release

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 1.1-5.git.0.ac7a99a
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Wed Dec 09 2015 Adam Miller <maxamillion@fedoraproject.org> - 1.1-4.git.0.ac7a99a
- Add iproute and procpc-ng Requires for sdn-ovs
- set .config_managed to %%ghost and %%config(noreplace)
- Fix dir ownership for redistributable clients
- Remove no longer needed basename reference

* Wed Dec 09 2015 Adam Miller <maxamillion@fedoraproject.org> - 1.1-3.git.0.ac7a99a
- Fix dir listing for kube_plugin_path

* Wed Dec 09 2015 Adam Miller <maxamillion@fedoraproject.org> - 1.1-2.git.0.ac7a99a
- Fix dir listing for sdn

* Wed Dec 09 2015 Adam Miller <maxamillion@fedoraproject.org> - 1.1-1.git.0.ac7a99a
- Remove no longer needed defattr
- Remove Obsoletes for package never in Fedora
- Remove upstream specific conditionals for el7aos dist tag

* Wed Dec 02 2015 Adam Miller <maxamillion@fedoraproject.org> - 1.1-0.git.0.ac7a99a
- First submission to Fedora

